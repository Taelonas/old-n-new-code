/*
VernierAnalogSensor (v 2014.09)
Reads a Vernier analog (BTA) Sensor connected to pin A0 of the Arduino. 
This sketch displays the time and sensor readings on the Serial Monitor. 
As written, the readings will be displayed every half second. 
Change the variable TimeBetweenReadings to change the rate.
 
 See www.vernier.com/arduino for more information.
*/

// Code modifed by Olli Kortelainen (olli.kortelainen@metropolia.fi)
 
/////////////////////////////////////////////////////////
// This is the information on the sensor being used.   //
// See the www.vernier.com/products/sensors.           //
/////////////////////////////////////////////////////////
char Sensor[]="Metropolia Signal Box";
float Intercept = -10; // This has to be -10 when using Voltage probe
float Slope = 4; //This has to be 4 when using Voltage probe
int TimeBetweenReadings = 500; // in ms
int ReadingNumber=0;

////////////////////////////////////////////////////////////////////////////////////////////
// setup() function runs only once when Arduino is powered. REQUIRED FOR ARDUINO TO WORK! //
////////////////////////////////////////////////////////////////////////////////////////////
void setup() 
{
	Serial.begin(9600); //initialize serial communication at 9600 baud
	while (!Serial);
	/*
	Serial.println("Vernier Format 2");
	Serial.print(Sensor);
	Serial.print(" ");
	Serial.println("Readings taken using Ardunio");
	Serial.println("Data Set");
	Serial.print("Time");//long name
	Serial.print("\t"); //tab character
	Serial.println ("Force"); //change to match sensor
	Serial.print("t");//short name
	Serial.print("\t"); //tab character
	Serial.println ("F"); //short name, change to match sensor 
	Serial.print("seconds");//units
	Serial.print("\t"); // tab character
	Serial.println ("newtons"); //change to match sensor
	*/
	
}

////////////////////////////////////////////////////////////////////////////////////////
// loop() function loops as long as arduino is powered. REQUIRED FOR ARDUINO TO WORK! //
////////////////////////////////////////////////////////////////////////////////////////
void loop() 
{
	float Time;
	
	//the print below does the division first to avoid overflows
	//Serial.print(ReadingNumber/1000.0*TimeBetweenReadings); 
	float Count = analogRead(A1); // Change this depending on what sensor port you are using.
	if (Count > 0.0)
	{
		digitalWrite(LED_BUILTIN, HIGH);
	}
	else
	{
		digitalWrite(LED_BUILTIN, LOW); 
	}
	Serial.println(Count);
	/*
	float Voltage = Count / 1023 * 5.0;// convert from count to raw voltage
	float SensorReading= Intercept + Voltage * Slope; //converts voltage to sensor reading
	Serial.print("\t"); // tab character
	Serial.println(SensorReading);
	*/
	delay(TimeBetweenReadings);// delay in between reads for stability
	ReadingNumber++;
}