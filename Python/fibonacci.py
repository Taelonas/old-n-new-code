import time


def nth_fibonacci(max_n):
    start = time.time()
    # First two numbers required in the sequence
    num1, num2 = 0, 1

    # Incase user inputs one of the two first numbers
    if max_n == 0:
        return "Loops: " + str(0) + " Number:" + str(num1)
    if max_n == 1:
        return "Loops: " + str(1) + " Number:" + str(num2)
    if max_n < 0:
        return "No negative numbers in the Fibonacci sequence."

    # Calculate the next number in the sequence and update the last two numbers
    for i in range(2, max_n):
        num3 = num1 + num2
        num1, num2 = num2, num3
        loops = i

    end = time.time()
    runtime = end - start
    return "Loops: " + str(loops) + " Runtime: " + str(runtime) + " Number:" + str(num3)


print(nth_fibonacci(0))
print(nth_fibonacci(1))
print(nth_fibonacci(5))
print(nth_fibonacci(10))
print(nth_fibonacci(50))
print(nth_fibonacci(100))
print(nth_fibonacci(15000))
print(nth_fibonacci(-1))
