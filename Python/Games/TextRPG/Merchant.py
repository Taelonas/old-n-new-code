import random
from Weapon import Weapon


class Merchant(object):
    def __init__(self):
        pass

    def repair(self, weapon):
        weapon.repair()
        return weapon

    def talk(self):
        print("Merchant: Piss off!")

    def trade(self, weapon):
        loop = True
        count = 0

        def askInput(inputSize):
            loop = True
            while loop:
                choice = int(input("What do you want to do? ").strip())
                print("You choose: %d" % (choice))

                if choice <= inputSize and choice >= 1 and isinstance(choice, int):
                    loop = False
                    return choice

        while loop:
            print("Merchant: What weapon do you want?")
            print("1 = Dagger (DMG=1-2)")
            print("2 = Sword (DMG=2-3)")
            print("3 = Mace (DMG=3-4)")
            print("4 = Axe (DMG=4-5)")
            choice = askInput(4)

            if choice == 1:
                weapon = Weapon("dagger")
                loop = False
            elif choice == 2:
                weapon = Weapon("sword")
                loop = False
            elif choice == 3:
                weapon = Weapon("mace")
                loop = False
            elif choice == 4:
                weapon = Weapon("axe")
                loop = False
            elif count >= 3:
                print("Merchant: Forget it you idiot!")
                loop = False
            else:
                print("Merchant: What the hell are you doing?!")
                count += 1

        return weapon
