from Player import Player
from Merchant import Merchant

HEALTH = 100
STAMINA = 100
SEX = "Male"

game = True
print("Welcome to TextRPG - A simple text-based game")
player = Player(HEALTH, STAMINA, SEX)
merchant = Merchant()


def askInput(inputSize):
    loop = True
    while loop:
        choice = int(input("What do you want to do? ").strip())
        print("You choose: %d" % (choice))

        if choice <= inputSize and choice >= 1 and isinstance(choice, int):
            loop = False
            return choice


while game:
    print("You are in a boring town.")
    print("There is only a merchant around and nobody else.")
    print("What do you want to do?")
    print("1 - Talk to merchant")
    print("2 - Fight a shadow")
    print("3 - Quit game")
    choice = askInput(3)

    if choice == 3:
        print("Game Ended!")
        game = False

    elif choice == 1:
        loop = True
        while loop:
            print("What do you want to do?")
            print("1 - Repair weapon.")
            print("2 - Trade weapon.")
            print("3 - Talk.")
            print("4 - Back to street.")
            choice = askInput(4)

            if choice == 1:
                merchant.repair(player.weapon)
            elif choice == 2:
                merchant.trade(player.weapon)
            elif choice == 3:
                merchant.talk()
            elif choice == 4:
                print("You went back to the street.")
                loop = False
            else:
                print("Invalid input!")

    elif choice == 2:

        def printStats(health, stamina):
            print("Health: " + str(health))
            print("Stamina: " + str(stamina))

        print("Started fighting a shadow")
        loop = True
        shadowHealth = 50
        shadowStamina = 3
        shadowMinDam = 1
        shadowMaxDam = 2
        while loop:
            if shadowHealth >= 50:
                print("The shadow is staring at you.")
                printStats(shadowHealth, shadowStamina)
            elif shadowHealth < 50 and shadowHealth > 25:
                print("The shadow is wounded.")
                printStats(shadowHealth, shadowStamina)
            elif shadowHealth < 25 and shadowHealth > 0:
                print("The shadow is greatly wounded.")
                printStats(shadowHealth, shadowStamina)
            else:
                print("You beat the shadow! Hooray!")
                printStats(shadowHealth, shadowStamina)
                loop = False

            print("1 - Attack")
            print("2 - Rest (Increase STAMINA)")
            print("3 - Heal (Increase HEALTH)")
            print("4 - Flee (Leave COMBAT)")
            choice = askInput(4)

            if choice == 1:
                damage = player.weapon.hit()
                shadowHealth -= damage
                print("You hit the shadow for %d damage." % (damage))

            elif choice == 2:
                print("You rested.")
                player.rest()
                printStats(player.HEALTH, player.STAMINA)
            elif choice == 3:
                print("You healed.")
                player.heal()
                printStats(player.HEALTH, player.STAMINA)
            elif choice == 4:
                print("You flead...pussy")
                loop = False
