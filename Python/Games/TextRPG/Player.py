import random
from Weapon import Weapon

MAXHEALTH = 100
MAXSTAMINA = 100


def generateWeapon():
    x = random.randint(1, 5)

    if x == 1:
        weapon = Weapon("dagger")
    elif x == 2:
        weapon = Weapon("sword")
    elif x == 3:
        weapon = Weapon("mace")
    elif x == 4:
        weapon = Weapon("axe")

    print(weapon)
    return weapon


class Player(object):
    def __init__(self, HEALTH, STAMINA, SEX):
        self.HEALTH = HEALTH
        self.STAMINA = STAMINA
        self.SEX = SEX
        self.weapon = generateWeapon()

    def __del__(self):
        print("You died")

    def attack(self):
        self.STAMINA -= 5
        return self.weapon.hit()

    def getHit(self, damage):
        self.HEALTH -= damage
        self.STAMINA -= 5
        if self.HEALTH <= 0:
            self.HEALTH = 0
            return ("You took %d damage. You died!" % (damage))
        else:
            returnString = "You took %d damage.\n" % (damage)
            returnString += "You have %d health left.\n" % (self.HEALTH)
            return returnString

    def rest(self):
        staminaIncrease = 5
        if self.STAMINA >= MAXSTAMINA:
            pass
        elif (self.STAMINA < MAXSTAMINA) and (self.STAMINA + staminaIncrease > MAXSTAMINA):
            self.STAMINA = MAXSTAMINA
        else:
            self.STAMINA += 5

    def heal(self):
        healthIncrease = 5
        if self.HEALTH >= MAXHEALTH:
            pass
        elif (self.HEALTH < MAXHEALTH) and (self.HEALTH + healthIncrease > MAXHEALTH):
            self.HEALTH = MAXHEALTH
        else:
            self.HEALTH += 5

    def giveWeapon(self):
        return self.weapon
