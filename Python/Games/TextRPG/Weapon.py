import random


class Weapon(object):
    def __init__(self, newWeapon):
        if newWeapon == "dagger":
            self.weapon = newWeapon
            self.minDamage = 1
            self.maxDamage = 2
            self.durability = 50
            self.defaultDurability = self.durability
        elif newWeapon == "sword":
            self.weapon = newWeapon
            self.minDamage = 2
            self.maxDamage = 3
            self.durability = 25
            self.defaultDurability = self.durability
        elif newWeapon == "mace":
            self.weapon = newWeapon
            self.minDamage = 3
            self.maxDamage = 4
            self.durability = 20
            self.defaultDurability = self.durability
        elif newWeapon == "axe":
            self.weapon = newWeapon
            self.minDamage = 4
            self.maxDamage = 5
            self.durability = 15
            self.defaultDurability = self.durability

    def returnDurability(self):
        return self.durability

    def lowerDurability(self, x):
        self.durability -= x
        if self.durability < 0:
            self.durability = 0
        return self.durability

    def repair(self):
        self.durability = self.defaultDurability

    def hit(self):
        if self.durability <= 0:
            return 0
        else:
            return random.randint(self.minDamage, self.maxDamage+1)

    def weaponDescription(self):
        returnString = "Weapon: %s\n" % (self.weapon)
        returnString += "Damage: %d-%d\n" % (self.minDamage, self.maxDamage)
        returnString += "Durability: %d\n" % (self.durability)
        return returnString
