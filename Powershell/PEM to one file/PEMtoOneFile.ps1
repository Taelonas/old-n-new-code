﻿$dateFormat = "ddMMyyyy_hh.mm"
$date = (Get-Date).ToString($dateFormat)

$inputPath = "$PSScriptRoot\PEM" 
$files = Get-ChildItem $inputPath\*.pem

$outputPath = "$PSScriptRoot\Output"
$newfile = "$outputPath\Bundle_$date.pem"



foreach ($file in $files ){
    Get-Content $file | Add-Content $newfile
}