﻿# Author: Olli Kortelainen / HUS Tietohallinto
# Script Updated: 15.3.2019

# Set folders and files
$inputFolder = "$PSScriptRoot\input"                 # Path to Input folder
$outputFolder = "$PSScriptRoot\output"               # Path to Output folder
$tempFolder = "$PSScriptRoot\temp"                   # Path to Temp folder
$templatesFile = "$inputfolder\templates.txt"        # Template file. Add Certificate templates OID and separate with line-break
$finalFile = "$outputFolder\finalfileName.csv"       # Location and naming of final file
$EfecteUploadServer = "\\COMPUTERNAME\c$\FOLDERNAME" # Address to where the files should be uploaded


# Date and date format
$dateFormat = "dd/MM/yyyy hh:mm"
$pastDate = (Get-Date).AddDays(-30).ToString($dateFormat) # Checks only the last 30 days of issued/revoked certificates

$templates = Get-Content -Path $templatesFile # Read line-break separated certificate OID templates from file


function Private:main
{
    RunCertutilCommand(20) # Create lists of Issued certificates
    RunCertutilCommand(21) # Create lists of Revoked Certificates
    CombineTempFilesToSingleFile
    SortByRequestID
    CopyToServer
    DeleteTempFolderCSVFiles
}


function RunCertutilCommand($disposition)
{
    $n = 1 # Used to name tempfiles
    ForEach ($template in $global:templates)
    {
    
        # Certutil restrictions
        $certutilRestrictions = "Disposition=$disposition,NotBefore>=$global:pastDate,CertificateTemplate=$template"

        # Certutil output
        $certutilOutput = "StatusCode,Request ID,Issued Common Name,Serial Number,Certificate Effective Date,Certificate Expiration Date,CertificateTemplate,Public Key Length,Requester Name"

        # Run certutil command
        [System.Void](& certutil -view -restrict $certutilRestrictions -out $certutilOutput csv > "$global:tempFolder\disp-$disposition-temp$n.csv")

        $n += 1
    }
}

function CombineTempFilesToSingleFile
{
    $first = $true # Used only for first iteration of following loop
    Get-ChildItem -Path $global:tempFolder -Filter *.csv |
    Foreach-Object {
        $tempFile = $_.FullName
    

        if ($first) # Removes Date from the beginning of the file 
        {
            .{ "$global:certutilOutput" 
            Get-Content $tempFile | Select-Object -Skip 1 } | Set-Content $global:tempFolder\finaltemp.csv

            $first = $false
        }
        else # Removes first line of file
        {
            (Get-Content $tempFile | Select-Object -Skip 1) | Add-Content $global:tempFolder\finaltemp.csv
        }

    }
}

function SortByRequestID
{
    # Sort according to Request ID
    Import-Csv -Path $global:tempFolder\finaltemp.csv | sort "Request ID" | Export-Csv -Path $global:finalFile -NoTypeInformation 
}

function CopyToServer
{
    # Copy final file to Efecte server for upload
    Copy-Item –Path $global:finalFile –Destination $global:EfecteUploadServer
}

function DeleteTempFolderCSVFiles
{
    # Remove Temp files
    Remove-Item –path $global:tempFolder\* -include *.csv
}


main