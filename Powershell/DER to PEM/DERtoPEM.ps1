﻿$dateFormat = "ddMMyyyy_hh.mm"
$date = (Get-Date).ToString($dateFormat)

$DERPath = "$PSScriptRoot\DER" 
$files = Get-ChildItem $DERPath

$PEMPath = "$PSScriptRoot\PEM"
$PEMOutputFolderName = "PEM_$date"


New-Item -Path $PEMPath -Name $PEMOutputFolderName -ItemType "directory"

foreach ($file in $files ){
    $basename = $file | select -expand basename 
    certutil -encode $DERPath\$file $PEMPath\$PEMOutputFolderName\$basename.pem
}