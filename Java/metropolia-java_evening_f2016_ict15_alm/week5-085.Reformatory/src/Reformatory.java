public class Reformatory {
    
     private int weight;

    public int weight(Person person)
    {
        int weight = person.getWeight();

        this.weight++;

        // return the weight of the person
        return weight;
    }
    public void feed(Person person)
    {
        //that increases the weight of its parameter by one.
        person.setWeight(person.getWeight() + 1);

    }
    public int totalWeightsMeasured()
    {
        return weight;
    }
}

