
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Student> list = new ArrayList<Student>();
        /*Student pekka = new Student("Pekka Mikkola", "013141590");
        System.out.println("name: " + pekka.getName());
        System.out.println("studentnumber: " + pekka.getStudentNumber());
        System.out.println(pekka); */
        
        boolean loop = true;
        String name,studentNumber,search;
        Scanner reader = new Scanner(System.in);
        int i = 0;
        
        while (loop==true)
        {
            System.out.print("name: ");
            name = reader.nextLine();
            
            if (name.isEmpty())
            {
                loop = false;
            }
            else
            {
                System.out.print("studentnumber: ");
                studentNumber = reader.nextLine();
                Student students = new Student(name, studentNumber);
                list.add(students);
            }
            
            
        }
        
        for (Student student : list)
        {
            System.out.println(student);
        }
        
        System.out.print("Give search term: ");
        search = reader.nextLine();
        
        System.out.println("Result:");
        for (Student student : list)
        {
            if (student.getName().contains(search))
            {
                System.out.print(student);
            }
            else
            {
                
            }
        }
        
    }
}