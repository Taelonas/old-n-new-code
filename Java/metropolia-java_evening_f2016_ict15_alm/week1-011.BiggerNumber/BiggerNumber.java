
import java.util.Scanner;

public class BiggerNumber {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        int x,y;
        
        System.out.print("Give a number: ");
        x = Integer.parseInt(reader.nextLine());
        System.out.print("Give another number: ");
        y = Integer.parseInt(reader.nextLine());
        
        if (x > y)
            System.out.print("The bigger number of the two numbers given was: "+ x);
        else
            System.out.print("The bigger number of the two numbers given was: "+ y);

    }
}
