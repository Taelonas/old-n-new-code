
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        // write testcode here
        
        int[] values = {8, 3, 7, 9, 1, 2, 4};
        sort(values);
    }
    
    public static int smallest(int[] array) {
    // write the code here
        int i,j,smallest;
        smallest=array[0];
        
        for (i=0; i<array.length; i++) 
        {
            if (smallest <= array[i]) 
            {
                
            } 
            else 
            {
                smallest = array[i];
            }
        }
        return smallest;
        
    }
    
    public static int indexOfTheSmallest(int[] array)
    {
        int i,j,smallest;
        smallest=array[0];
        
        for (i=0; i<array.length; i++) 
        {
            if (smallest <= array[i]) 
            {
                
            } 
            else 
            {
                smallest = i;
            }
        }
        return smallest;
    }
    
    public static int indexOfTheSmallestStartingFrom(int[] array, int index) {
        int small = array[index];
        for (int i = index; i < array.length; i++) {
            if (small <= array[i]) {

            } else {
                small = array[i];
                index = i;
            }
        }
        return index;
    }
    
    public static void swap(int[] array, int index1, int index2) 
    {
        // code goes here
        int indexValue1 = array[index1];
        int indexValue2 = array[index2];
        
        array[index1] = indexValue2;
        array[index2] = indexValue1;    
        
    }
    
    public static void sort(int[] array) 
    {
        for (int i = 0; i < array.length; i++) {
            System.out.println(Arrays.toString(array));
            int smallestIndex = indexOfTheSmallestStartingFrom(array, i);
            swap(array, i, smallestIndex);
        }
    }
}
  
    

