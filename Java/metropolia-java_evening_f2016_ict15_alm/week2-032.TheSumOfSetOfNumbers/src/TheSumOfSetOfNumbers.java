
import java.util.Scanner;

public class TheSumOfSetOfNumbers {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        
        int rounds;
        int round = 1;
        int sum = 0;
        System.out.print("Until what? ");
        rounds = Integer.parseInt(reader.nextLine());
        
        while (round <= rounds)
        {
            sum = sum + round;
            round++;
        }
        
        System.out.print("Sum is " + sum);

    }
}
