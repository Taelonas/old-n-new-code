
import java.util.Scanner;


public class UpToCertainNumber {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        
        int InputNumber, number = 1;
        
        System.out.print("Up to what number? ");
        InputNumber = Integer.parseInt(reader.nextLine());
        
        while (number <= InputNumber)
        {
            System.out.println(number);
            number = number + 1;
        }
        
    }
}
