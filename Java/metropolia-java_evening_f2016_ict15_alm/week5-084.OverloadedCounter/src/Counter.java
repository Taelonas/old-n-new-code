/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author korte
 */
public class Counter 
{
    private int value;
    private boolean new_check;
    
    
    public Counter(int startingValue, boolean check)
    {
        this.value = startingValue;
        this.new_check = check;
        
        
    }
    
    public Counter(int startingValue)
    {
        this.value = startingValue;
        this.new_check=false;
    }
    
    public Counter(boolean check)
    {
        this.value = 0;
        this.new_check=check;
    }
    
    public Counter()
    {
        this.value = 0;
        this.new_check=false;
    }
    
    
    public int value()
    {
        return this.value;
    }
    
    public void increase(int increaseAmount)
    {
        if (increaseAmount < 0)
        {
            
        }
        else
        {
            
            this.value = this.value + increaseAmount;
        }
        
    }
    
    public void increase()
    {
        this.value++;    
    }
    
    public void decrease(int decreaseAmount)
    {
        if (this.new_check == false)
        {
            if (decreaseAmount < 0)
            {
                
            }
            else
            {
                this.value = this.value - decreaseAmount;
            }
            
            
        }
        else
        {
            this.value = this.value - decreaseAmount;
            if (this.value < 0)
            {
                this.value=0;
            }
            else{
                
            } 
            
            
        }
    }
    
    public void decrease()
    {
        if (this.new_check == false)
        {
            this.value--;  
        }
        else
        {
            this.value--;
            if (this.value < 0)
            {
                this.value=0;
            }
            else{
                
            } 
            
            
        }
    }
    
}
