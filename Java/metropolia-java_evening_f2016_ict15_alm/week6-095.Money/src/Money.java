
public class Money {

    private final int euros;
    private final int cents;

    public Money(int euros, int cents) {

        if (cents > 99) {
            euros += cents / 100;
            cents %= 100;
        }

        this.euros = euros;
        this.cents = cents;
    }

    public int euros() {
        return euros;
    }

    public int cents() {
        return cents;
    }

    @Override
    public String toString() {
        String zero = "";
        if (cents < 10) {
            zero = "0";
        }

        return euros + "." + zero + cents + "e";
    }
    
    public Money plus(Money added)
    {
        int euroSum;
        int centsSum;
        
        euroSum = this.euros + added.euros();
        centsSum = this.cents + added.cents();
        
        Money newSum = new Money(euroSum,centsSum);
        
        return (newSum);
        
    }
    
    public boolean less(Money compared)
    {
        if (this.euros() == compared.euros())
        {
            if (this.cents() == compared.cents())
            {
                return true;
            }
            else if (this.cents() > compared.cents())
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else if (this.euros() > compared.euros())
        {
            return false;
        }
        else
        {
            return true;
        }
        
    }
    
    public Money minus(Money decremented)
    {
        int euroSumInCents;
        int centsSum;
        
        euroSumInCents = 100*(this.euros - decremented.euros());
        centsSum = this.cents - decremented.cents();
        

        euroSumInCents += centsSum;
        
        if (euroSumInCents<0)
        {
            euroSumInCents = 0;
        }
        
        Money newSum = new Money(0,euroSumInCents);
        
        return (newSum);
    }

}
