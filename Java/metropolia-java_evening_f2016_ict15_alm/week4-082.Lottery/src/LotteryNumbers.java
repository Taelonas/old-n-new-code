import java.util.ArrayList;
import java.util.Random;

public final class LotteryNumbers 
{
    private ArrayList<Integer> numbers;
    private Random random = new Random();
    
    

    public LotteryNumbers() 
    {
        // We'll format a list for the numbers
        this.numbers = new ArrayList<Integer>();
        // Draw numbers as LotteryNumbers is created
        this.drawNumbers();
    }

    public ArrayList<Integer> numbers() 
    {
        return this.numbers;
    }

    public void drawNumbers() 
    {
        // Write the number drawing here using the method containsNumber()
        int maxNumber = 39;
        int minNumber = 1;
        int loop = 1;
        boolean check;
        int number;
        
        this.numbers.removeAll(numbers);
        
        while (loop <= 7)
        {
            
            number = random.nextInt((maxNumber - minNumber)+ 1) + minNumber;
            check = containsNumber(number);
            if (check == true)
            {
                
            }
            else
            {
                this.numbers.add(number);
                loop++;
            }
        }
        
        
        
        
        
        
    }

    public boolean containsNumber(int number) 
    {
        // Test here if the number is already among the drawn numbers
        if (this.numbers.contains(number))
        {
            
            return true;
        }
        else
        {
            return false;
        }
    }
}
