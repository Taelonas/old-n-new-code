import java.util.ArrayList;
import java.util.Scanner;

public class Words
{
    public static void main(String[] args)
    {
        Scanner reader = new Scanner(System.in);
        ArrayList<String> words = new ArrayList<String>();
        
        int program = 1;
        int i = 0;
        int j = 0;
        
        while (program == 1)
        {
            System.out.println("Type a word: ");
            String text = reader.nextLine();
            
            if (text.isEmpty())
            {
                for (String word : words) 
                {
                 System.out.println( word );
                }
                program=0;
            }
            else
            {
                words.add(text);
                i++;
            }
        }
    }
}