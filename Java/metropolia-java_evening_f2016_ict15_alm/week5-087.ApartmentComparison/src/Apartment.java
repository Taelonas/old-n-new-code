
public class Apartment {

    private int rooms;
    private int squareMeters;
    private int pricePerSquareMeter;

    public Apartment(int rooms, int squareMeters, int pricePerSquareMeter) {
        this.rooms = rooms;
        this.squareMeters = squareMeters;
        this.pricePerSquareMeter = pricePerSquareMeter;
    }
    
    public boolean larger(Apartment otherApartment)
    {
        if (this.squareMeters> otherApartment.squareMeters)
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
    
     public int priceDifference(Apartment otherApartment)
     {
         int apartmentPrice = this.squareMeters * this.pricePerSquareMeter;
         int otherApartmentPrice = otherApartment.squareMeters * otherApartment.pricePerSquareMeter;
         int sum=0;
         
         if (apartmentPrice > otherApartmentPrice)
         {
            sum = apartmentPrice - otherApartmentPrice;             
         }
         else
         {
             sum = otherApartmentPrice - apartmentPrice;
         }
         
         return sum;
     }
     
     public boolean moreExpensiveThan(Apartment otherApartment)
     {
         int apartmentPrice = this.squareMeters * this.pricePerSquareMeter;
         int otherApartmentPrice = otherApartment.squareMeters * otherApartment.pricePerSquareMeter;
         
         if (apartmentPrice > otherApartmentPrice)
         {
            return true;             
         }
         else
         {
             return false;
         }
         
     }
}
