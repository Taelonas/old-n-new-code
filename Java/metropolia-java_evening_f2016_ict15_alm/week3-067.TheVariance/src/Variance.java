import java.util.ArrayList;

public class Variance 
{
    // Copy here sum from exercise 63 
   public static int sum(ArrayList<Integer> list) 
   {
        int sum = 0;
        
        for (int numbers : list)
        {
            sum = sum + numbers;
        }
        return sum;
    }
    

    public static double average(ArrayList<Integer> list) 
    {
        // write code here
        double average = 0;
        int i = 0;
        double summa = 0;
        
        for (int numbers : list)
        {
            i++;
        }
        
        summa = sum(list);
        average = summa/i;

        return average;
    }

    public static double variance(ArrayList<Integer> list) 
    {
        // write code here
        double number = 0; 
        double ave = average(list);
        for (int i:list)
        {
            number = number + Math.pow((i - ave), 2.0);
        }
        double denom = (double)(list.size()) - 1.0;
        double var = number/denom;
        return var;

    }
    
    public static void main(String[] args) 
    {
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(3);
        list.add(2);
        list.add(7);
        list.add(2);
        
        System.out.println("The variance is: " + variance(list));
    }

}
