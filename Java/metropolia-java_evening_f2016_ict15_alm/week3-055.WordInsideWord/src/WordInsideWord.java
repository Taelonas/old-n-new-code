
import java.util.Scanner;

public class WordInsideWord {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        
        System.out.print("Type the first word: ");
        String wordFirst = reader.nextLine();
        System.out.print("Type the second word: ");
        String wordSecond = reader.nextLine();
        int index = wordFirst.indexOf(wordSecond);
        
        if (index == -1)
        {
            System.out.print("The word '"+wordSecond+"' is not found in the word '"+wordFirst+"'.");
        }
        else
        {
            System.out.print("The word '"+wordSecond+"' is found in the word '"+wordFirst+"'.");
        }
            
        
    }
}
