import java.util.Random;

public class PasswordRandomizer {
    private final Random random = new Random();
    // Define the 
    int pwLength;
    String pWord = null;

    public PasswordRandomizer(int length) {
        // Initialize the variable
        this.pwLength = length;
    }

    public String createPassword() {
        // write code that returns a randomized password
        int maxNumber = 25;
        int minNumber = 1;
        int number;
        pWord = "";
        
        for (int j=0;j<this.pwLength;j++)
        {
            number = (random.nextInt(maxNumber - minNumber + 1) + minNumber);
            char symbol = "abcdefghijklmnopqrstuvwxyz".charAt(number);
            pWord = pWord + symbol;
        }
        
        
        
        return pWord;
    }
}
