
import java.util.Scanner;

public class SumOfThePowers {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        
        int round = 1;
        int power;
        int sum = 1;
        
        System.out.print("Type a number: ");
        power = Integer.parseInt(reader.nextLine());
        
        while (round <= power)
        {
            sum = sum + (int)Math.pow(2,round);
            round++;           
            
        }
        System.out.print("The result is " + sum);      
        

    }
}
