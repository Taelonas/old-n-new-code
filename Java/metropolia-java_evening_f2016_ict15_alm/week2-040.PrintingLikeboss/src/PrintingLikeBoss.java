public class PrintingLikeBoss {

    // copy or rewrite the method of Assignment 39.1 here
    public static void printStars(int amount) {
        
        int round = 1; 
        
        while (round <= amount)
        {
            System.out.print("*");
            
            round++;
        }
        
        System.out.println("");        
    }

    public static void printWhitespaces(int amount) {
        
        int round = 1;
        
        while (round <= amount)
        {
            System.out.print(" ");
            round++;
        }
        // 40.1
    }

    public static void printTriangle(int size) {
        
        int round = 1;
        int space = size - 1;
        
        while (round <= size)
        {
            printWhitespaces(space);
            space--;
            printStars(round);
            round++;
        }
        // 40.2
    }

    public static void xmasTree(int height) {
        
        int round = 1;
        int space = height -1;
        int stars = 1;
        int stump = height - 2;
        
        while (round <= height)
        {
            printWhitespaces(space);
            space--;
            printStars(stars);
            stars = stars + 2;
            round++;
        }
        
        printWhitespaces(stump);
        printStars(3);
        printWhitespaces(stump);
        printStars(3);
        // 40.3
    }

    public static void main(String[] args) {
        // Tests do not use main, yo can write code here freely!

        printTriangle(5);
        System.out.println("---");
        xmasTree(4);
        System.out.println("---");
        xmasTree(10);
    }
}
