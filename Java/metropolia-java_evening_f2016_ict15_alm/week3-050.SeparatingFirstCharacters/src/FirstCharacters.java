import java.util.Scanner;

public class FirstCharacters {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.print("Type your name: ");
        String name = reader.nextLine();
        int x=0;
        int y=2;
        int number = 1;
        
        while (x <= y)
        {
            if (name.length() < 3 )
            {
                break;
            }
            else
            {
                System.out.println(number + ". character: " + name.charAt(x));
                number++;
                x++;
            }
            
        }
    }
}
