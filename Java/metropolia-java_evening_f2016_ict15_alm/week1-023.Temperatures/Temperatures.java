
import java.util.Scanner;

public class Temperatures {

    public static void main(String[] args) {

        Scanner reader = new Scanner(System.in);
        double value;
        
        while(true)
        {
            System.out.print("Enter a floating number: ");
            value = Double.parseDouble(reader.nextLine());
            
            if (value < -30 || value > 40)
            {
                continue;
            }
            
            else
            {
                Graph.addNumber(value);                
            }
            
            
        }

    }
}