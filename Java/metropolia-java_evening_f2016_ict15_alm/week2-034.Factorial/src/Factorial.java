import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        
        int number=1, inputNumber;
        int sum = 1;
        
        System.out.print("Type a number: ");
        inputNumber = Integer.parseInt(reader.nextLine());
        
        while (number <= inputNumber)
        {
            sum = sum * number;
            number++;
        }
        System.out.print("Factorial is " + sum);

    }
}
