
import java.util.Scanner;

public class Divider {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

       int X , Y;
       float Z;
       
       System.out.print("Type a number: ");
       X = Integer.parseInt(reader.nextLine());
       System.out.print("Type another number: ");
       Y = Integer.parseInt(reader.nextLine());
       
       Z = (float)X / Y;
       
       System.out.print("Division: " + X + "/" + Y + "=" + Z);
    }
}
