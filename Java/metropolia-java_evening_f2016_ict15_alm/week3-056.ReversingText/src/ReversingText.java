
import java.util.Scanner;

public class ReversingText {

    public static String reverse(String text) {
        int textLength = text.length() - 1;
        int character = textLength;
        int round = 0;
        String reverse = "";
        char charPlace;
        
        while (round <= textLength)
        {
            charPlace = text.charAt(character);
            character--;
            reverse = reverse + charPlace;
            round++;
        }
        return reverse;
    }

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.print("Type in your text: ");
        String text = reader.nextLine();
        System.out.println("In reverse order: " + reverse(text));
    }
}
