
public class HangmanLogic {

    private String word;
    private String guessedLetters;
    private int numberOfFaults;

    public HangmanLogic(String word) {
        this.word = word.toUpperCase();
        this.guessedLetters = "";
        this.numberOfFaults = 0;
    }

    public int numberOfFaults() {
        return this.numberOfFaults;
    }

    public String guessedLetters() {
        return this.guessedLetters;
    }

    public int losingFaultAmount() {
        return 12;
    }

    public void guessLetter(String letter) {
        // program here the functionality for making a guess
        if (this.guessedLetters.contains(letter))
        {
            
        }
        else
        {
           if (this.word.contains(letter))
           {
               this.guessedLetters += letter;
               
           }
           else
           {
               this.guessedLetters += letter;
               this.numberOfFaults++;
           }
        }

        // if the letter has already been guessed, nothing happens

        // it the word does not contains the guessed letter, number of faults increase
        // the letter is added among the already guessed letters
    }

    public String hiddenWord() {
        // program here the functionality for building the hidden word
        String HiddenWord = "";
        int i = this.word.length();
        int j=0;
        char a;
        
        while (j<i)
        {
            a = this.word.charAt(j);
            if (this.guessedLetters.contains("" + a))
            {
                HiddenWord += a;
            }
            else
            {
                HiddenWord = HiddenWord + "_";
            }
            j++;
        }
        
        return HiddenWord;
        // Declare empty Hidden word where start appending letters
    // Loop thru word we are trying to guess
        // If character at index i has been guessed already
            // PROTIP: You can check if String contains character by using wordString.contains("" + letterChar);
            // Add letter to Hidden word
        // Else if char hadn't been guessed append "_" to Hidden word
    // Return Hidden Word
          
        

        // create the hidden word by interating through this.word letter by letter
        // if the letter in turn is within the guessed words, put it in to the hidden word
        // if the letter is not among guessed, replace it with _ in the hidden word 

        // return the hidden word at the end
        
    }
}
