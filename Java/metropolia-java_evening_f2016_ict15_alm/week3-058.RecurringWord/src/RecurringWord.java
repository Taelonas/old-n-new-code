
import java.util.ArrayList;
import java.util.Scanner;

public class RecurringWord 
{

    public static void main(String[] args)
    {
        Scanner reader = new Scanner(System.in);
        // create here the ArrayList 
        ArrayList<String> words = new ArrayList<String>();
        
        int program = 1;
        
        while (program == 1)
        {
            System.out.print("Type a word: ");
            String text = reader.nextLine();
            
            if (words.contains(text))
            {
                System.out.println("You gave the word "+text+" twice");
                program=0;
            }
            else
            {
                words.add(text);
            }
            
        }
    }
}
