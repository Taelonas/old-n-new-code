
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class WordsInAlphabeticalOrder 
{

    public static void main(String[] args) 
    {
        Scanner reader = new Scanner(System.in);
        // create here an ArrayList
        ArrayList<String> words = new ArrayList<String>();
        
        int program = 1;
        int i = 0;
        int j = 0;
        
        while (program == 1)
        {
            System.out.print("Type a word: ");
            String text = reader.nextLine();
            
            if (text.isEmpty())
            {
                Collections.sort(words);
                for (String word : words) 
                {
                 System.out.println( word );
                }
                program=0;
            }
            else
            {
                words.add(text);
                i++;
            }
        }
    }
}
