
import java.util.Scanner;

public class Circumference {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        
        int radius;
        double circum;
        System.out.print("Type a radius: ");
        radius = Integer.parseInt(reader.nextLine());
        
        circum = 2 * Math.PI * radius;
        
        System.out.print("Circumference of the circle: " + circum);
        
    }
}
