import java.util.Scanner;

public class Palindromi 
{

    public static boolean palindrome(String text)
    {
        // write code here
        String reverseWord = reverse(text);
        if (reverseWord.equals(text))
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
    
    public static String reverse(String text) 
    {
        int textLength = text.length() - 1;
        int character = textLength;
        int round = 0;
        String reverse = "";
        char charPlace;
        
        while (round <= textLength)
        {
            charPlace = text.charAt(character);
            character--;
            reverse = reverse + charPlace;
            round++;
        }
        return reverse;
    }

    public static void main(String[] args) 
    {
        Scanner reader = new Scanner(System.in);
        
        System.out.println("Type a text: ");
        String text = reader.nextLine();    
        if (palindrome(text)) {
            System.out.println("The text is a palindrome!");
        } else {
            System.out.println("The text is not a palindrome!");
        }
    }
}
