
import java.util.Scanner;


public class TheSumBetweenTwoNumbers {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        
        int number1, number2;
        int sum = 0;
        
        System.out.print("First: ");
        number1 = Integer.parseInt(reader.nextLine());
        System.out.print("Second: ");
        number2 = Integer.parseInt(reader.nextLine());
        
        while (number1 <= number2)
        {
            sum = sum + number1;
            number1++;
        }
        
        System.out.print("Sum is " + sum);
    }
}
