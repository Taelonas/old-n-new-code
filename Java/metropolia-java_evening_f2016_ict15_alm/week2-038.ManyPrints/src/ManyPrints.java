
import java.util.Scanner;

public class ManyPrints {
    // NOTE: do not change the method definition, e.g. add parameters to method
    public static void printText() {
        System.out.println("In the beginning there were the swamp, the hoe and Java.");
    }

    public static void main(String[] args) {
        
        Scanner reader = new Scanner(System.in);
        int round = 1;
        int rounds;
        
        System.out.println("How many?");
        rounds = Integer.parseInt(reader.nextLine());
        
        while (round <= rounds)
        {
            printText();
            round++;
        }
        // ask the user how many times the text should be printed
        // use the while structure to call the printText method several times
        
    }
}