import java.util.Scanner;

public class LoopsEndingRemembering {
    public static void main(String[] args) {
        // program in this project exercises 36.1-36.5
        // actually this is just one program that is split in many parts
        
        Scanner reader = new Scanner(System.in);
        
        int sum = 0;
        int evenNumbers = 0;
        int oddNumbers = 0;
        int numbers = 0;
        double average;
        int inputNumber;
        
        
        System.out.println("Type numbers:");
        
        while(true)
        {
           inputNumber = Integer.parseInt(reader.nextLine());
           
           if (inputNumber == -1)
           {
               break;
           }
           else
           {
               sum = sum + inputNumber;
               numbers++;
               
               if (inputNumber % 2 == 0)
                   evenNumbers++;
               else
                   oddNumbers++;
                           
           }
           
        }
        
        System.out.println("Thank you and see you later!");
        System.out.println("The sum is " + sum);
        System.out.println("How many numbers: " + numbers);
        average = (double)sum / numbers;
        System.out.println("Average: " + average);
        System.out.println("Even numbers: " + evenNumbers);
        System.out.println("Odd numbers: " + oddNumbers);

    }
}
