import java.util.Scanner;
import java.util.ArrayList;
import java.util.Iterator;


public class Main {
    
    
    
    public static void main(String[] args) {
        Scanner lukija = new Scanner(System.in);
        ArrayList<Integer> list = new ArrayList<Integer>();
        // implement your program here
        // do not put all to one method/class but rather design a proper structure to your program
        
        // Your program should use only one Scanner object, i.e., it is allowed to call 
        // new Scanner only once. If you need scanner in multiple places, you can pass it as parameter
        
        boolean query = true;
        int score;
        
        String zero="";
        String one="";
        String two="";
        String three="";
        String four="";
        String five="";
        int[] numberArray = null;
        int j=0;
        
        int sum=0;
        double average=0.0;
        
        
        System.out.println("Type exam scores, -1 completes:");
        while(query == true)
        {
            //score = (read(lukija));
            score = Integer.parseInt(lukija.nextLine());
            if (score == -1)
            {
                query = false;
            }
            
            else if ((score <=29) && (score >= 0))
            {
                zero+="*";
            }
            else if ((score >= 30) && (score <= 34))
            {
                one+="*";
            }
            else if ((score >= 35) && (score <= 39))
            {
                two+="*";
            }
            else if ((score >= 40) && (score <= 44))
            {
                three+="*";
            }
            else if ((score >= 45) && (score <= 49))
            {
                four+="*";
            }
            else if ((score >= 50) && (score <= 60))
            {
                five+="*";
            }
            
        }
        
        System.out.println("Grade distribution:");
        System.out.println("5: " + five);
        System.out.println("4: " + four);
        System.out.println("3: " + three);
        System.out.println("2: " + two);
        System.out.println("1: " + one);
        System.out.println("0: " + zero);
        
        sum = one.length() + two.length() + three.length() + four.length() + five.length() + zero.length();
        
        if (sum == 0) {
            average = 0.0;
        } else 
        {
            average = (sum - zero.length()) * 100 / sum;
        }
        System.out.println("Acceptance percentage: " + average);
    }
        
        
        
}
   
    

    

