import java.util.Scanner;


public class LastCharacter {


    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.print("Type your name: ");
        String name = reader.nextLine();
        char lastChar = lastCharacter(name);
        System.out.print("Last character: " + lastChar);
       
    }
    
    public static char lastCharacter(String text)
    {
        int nameLength = text.length() - 1;
        char lastChar = text.charAt(nameLength);
        return lastChar;
    }
}
