/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author korte
 */
public class Bird {
    
    private String name;
    private String latinName;
    private int seen;
    
    public Bird (String name, String latinName)
    {
        this.name = name;
        this.latinName = latinName;
        this.seen = 0;
    }
    
    public void setSeen()
    {
        this.seen++;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public String getLatinName()
    {
        return this.latinName;
    }
    
    public int getSeen()
    {
        return this.seen;
    }
    
    public String toString()
    {
        return (this.name + " (" + this.latinName + "): " + this.seen + " observations");
    }
    
    
    
    
    
}
