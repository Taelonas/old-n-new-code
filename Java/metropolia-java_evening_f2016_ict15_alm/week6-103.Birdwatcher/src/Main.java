import java.util.Scanner;

public class Main {  

    public static void main(String[] args) 
    {
    // implement your program here
    // do not put all to one method/class but rather design a proper structure to your program
        
    // Your program should use only one Scanner object, i.e., it is allowed to call 
    // new Scanner only once. If you need scanner in multiple places, you can pass it as parameter
            
        Birdbook birdbook = new Birdbook();
        Scanner reader = new Scanner(System.in);
        
        String event;
        String name;
        String latinName;
        
        
        while(true)
        {
            System.out.print("? ");
            event = reader.nextLine();
            
            if (event.equalsIgnoreCase("Quit"))
            {
                break;
            }
            else if (event.equalsIgnoreCase("Add"))
            {
                System.out.print("Name: ");
                name = reader.nextLine();
                System.out.print("Latin Naem: ");
                latinName = reader.nextLine();
                
                birdbook.add(name, latinName);
            }
            else if (event.equalsIgnoreCase("Observation"))
            {
                System.out.print("What was observed:? ");
                name = reader.nextLine();
                
                birdbook.Observation(name);
            }
            
            else if (event.equalsIgnoreCase("Statistics"))
            {
                birdbook.Stats();
            }
            
            else if (event.equalsIgnoreCase("Show"))
            {
                System.out.print("What? ");
                name = reader.nextLine();
                birdbook.Show(name);
            }
            
        }
        
        
    }

}
