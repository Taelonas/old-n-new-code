
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author korte
 */

import java.util.ArrayList;

public class Birdbook {
    
    private ArrayList<Bird> birdbook = new ArrayList<Bird>();
    
    public void add(String name, String latinName) {
        Bird bird = new Bird(name, latinName);
        this.birdbook.add(bird);
    }
    
    public void Observation(String name)
    {
        
        for (Bird bird : this.birdbook) 
        {
            if (bird.getName().equalsIgnoreCase(name) || bird.getLatinName().equalsIgnoreCase(name)) 
            {
                bird.setSeen();
                return;
            }   
        }
        
        System.out.print("Is not a bird!");
        
    }
    
    public void Stats()
    {
        for (Bird bird : this.birdbook)
        {
            System.out.println(bird);
        }
    }
    
    public void Show(String name)
    {
        for (Bird bird : this.birdbook) 
        {
            if (bird.getName().equalsIgnoreCase(name) || bird.getLatinName().equalsIgnoreCase(name))
            {
                System.out.println(bird);
                return;
            }
        }

        System.out.print("No such bird");

    }
    
    
}
