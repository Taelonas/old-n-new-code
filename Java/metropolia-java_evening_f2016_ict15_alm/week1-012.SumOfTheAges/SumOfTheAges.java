
import java.util.Scanner;

public class SumOfTheAges {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        String name_1 , name_2;
        int age_1, age_2, sum;
        
        System.out.print("Type your name: ");
        name_1 = reader.nextLine();
        System.out.print("Type your age: ");
        age_1 = Integer.parseInt(reader.nextLine());
        System.out.print("");
        System.out.print("Type your name: ");
        name_2 = reader.nextLine();
        System.out.print("Type your age: ");
        age_2 = Integer.parseInt(reader.nextLine());
        System.out.print("");
        
        sum = age_1 + age_2;
        
        System.out.print(name_1 + " and " + name_2 + " are " + sum + " years old in total.");
    }
}
