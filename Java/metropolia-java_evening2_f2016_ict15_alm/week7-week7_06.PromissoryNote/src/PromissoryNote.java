
import java.util.HashMap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author korte
 */
public class PromissoryNote {
    
    private final HashMap<String,Double> loanNote = new HashMap<String, Double>();
    
    public PromissoryNote()
    {
        
    }
    
    public void setLoan(String toWhom, double value)
    {
        loanNote.put(toWhom, value);
    }
    
    public double howMuchIsTheDebt(String whose)
    {
        if (loanNote.containsKey(whose) == false)
        {
            return 0;
        }
        else
        {
            return loanNote.get(whose);
        }
        
    }
    
}
