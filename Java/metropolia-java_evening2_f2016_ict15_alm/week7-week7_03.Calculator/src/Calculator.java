/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author korte
 */


public class Calculator 
{
    
    private final Reader reader;
    private int count;
    
    public Calculator()
    {
        this.reader = new Reader();
        this.count = count;
    }
    
    public void start() 
    {
        while (true) 
        {
            System.out.print("command: ");
            String command = reader.readString();
            if (command.equals("end")) 
            {
                break;
            }

            if (command.equals("sum")) 
            {
                sum();
                count++;
            } else if (command.equals("difference")) 
            {
                difference();
                count++;
            } else if (command.equals("product")) 
            {
                product();
                count++;
            }
        }

        statistics();
    }
    
    private void sum() 
    {
       System.out.print("value1: ");
       int value1 = reader.readInteger(); // read the value using the Reader-object
       System.out.print("value2: ");
       int value2 = reader.readInteger(); // read the value using the Reader-object
       // print the value according to the exampl,e above
       int sum = value1 + value2;
        System.out.println("Sum of the values " + sum);
    }
    
    private void difference() 
    {
       System.out.print("value1: ");
       int value1 = reader.readInteger(); // read the value using the Reader-object
       System.out.print("value2: ");
       int value2 = reader.readInteger(); // read the value using the Reader-object
       // print the value according to the exampl,e above
       int difference = value1 - value2;
       
        System.out.println("Difference of the values " + difference);
    }
    
    private void product() 
    {
       System.out.print("value1: ");
       int value1 = reader.readInteger(); // read the value using the Reader-object
       System.out.print("value2: ");
       int value2 = reader.readInteger(); // read the value using the Reader-object
       // print the value according to the exampl,e above
       int product = value1 * value2;
       
        System.out.println("Product of the value " + product);
       
    }
    
    private void statistics() 
    {
        System.out.println("Calculations done " + count);
    }
 
   
}
