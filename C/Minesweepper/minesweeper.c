/*
 *
 *		Programming TI00AA43-3002
 *
 *
 *		Name: Olli Kortelainen
 *
 *		Student number: 1504169
 *
 *		Exercise: Project Minesweeper
 *		
 *		Build a minesweeper game (http://en.wikipedia.org/wiki/Minesweeper_%28video_game%29).
 *		Atleast 10 x 10 grid and possibility to mark found mines. Test the game to see how it works and	do	
 *		your best to implement the same functionality in your project.
 * 
 *		Number of mines is equal to 1/3 of the amount of square.
 *
 *		TO DO:
 *
 *		- Random function needs configuring. Is not fully random. 
 *		- Complete the game over function.
 *
 *
 *
 *
 */

 /* Importing the libraries */
 
#include <stdio.h>
#include <time.h>
#include <stdlib.h>


/* 1 column and 1 row will be used to print out the cordinates for the user.
 * Level 0 has the mines ,and level 1 hides the mines.  
 * I used the values 12 and 3 just incase I needed them. */

#define BOARD_SIZE 12
#define BOARD_LEVELS 3

/* Prototype functions */

void start_game();
void build_board();
void place_mines();
int random_number();
void hide_mines();
int player_input();
int game_status();
int check_for_mines(char row_input,char column_input,int flag,int remove_flag);
void dump_line();
void recursion(int x, int y);

/* Global variables */

char game_board[BOARD_SIZE][BOARD_SIZE][BOARD_LEVELS];

int main()
{
	char play_or_nay;
	int game_is_on = 1; 
	
	printf("\n\n");
	printf("********** Minesweeper **********\n");
	printf("* Copyright(c) Olli Kortelainen *\n");
	printf("* Metropolia school project     *\n");
	printf("*********************************\n");
	printf("\n\n");
	
	srand( time(0) ); 
	
	while (game_is_on == 1)
	{
		printf("Do you want to play minesweeper (Y/N)? ");
		
		while ((play_or_nay = getchar()) != '\n')
		{
			if (play_or_nay == 'y' || play_or_nay == 'Y')
			{	start_game();	}
		
			else if (play_or_nay == 'n' || play_or_nay == 'N' )
			{	game_is_on = 0;		}
		
			else
			{	printf("\n\nYour answer was invalid. Try again\n");
				printf("Do you want to play minesweeper (Y/N)? ");
			}
			
		}
		
	}
	
	printf("\n\nThank you for playing!\n");
	printf("Goodbye!!\n\n");
	
	return 0;
} 

/* Function that directs the game */

void start_game()
{
	int i=0;
	int j=0;
	int game_over=2;
	
	build_board();
	place_mines();
	hide_mines();
	
	/* Game is now ready to be played */
	
	
	
	while (game_over == 2)
	{
		game_over = game_status();
		
		if (game_over == 1)
		{}
		else if (game_over==2)
		{
			game_over = player_input();
		}
	}
	
	if (game_over == 0)
	{
		printf("Congratulations you won the game!");
	}
	else if (game_over == 1)
	{
		printf("\n------------------------------\n");
		printf("You hit a mine! Too bad :( ");
	}
	else
	{}
	
}


/* This function builds the base of the gameboard. cordinate system for when the player wants to target a space */

void build_board()
{
	int i=0;
	int j=0;
	char column_cord = 48;
	char row_cord = 97;
	
	for (i=0; i<3; i++)
	{
	game_board[0][0][i] = ' ';
	}
	
	for (i=0; i<3; i++)
	{
		for (j=1; j<11; j++)
		{
			game_board[0][j][i] = column_cord;
			column_cord++;
		}
		column_cord = 48;
		
	}
	
	for (i=0; i<3; i++)
	{
		for (j=1; j<11; j++)
		{
			game_board[j][0][i] = row_cord;
			row_cord++;
		}
		
		row_cord = 97;
	}
	
	for (i=1;i<=11;i++)
	{
		for (j=1;j<11;j++)
		{
			game_board[j][i][0] = ' ';
		}
		
	}
}

/* Function to place the mines onto the gameboard. The mines will be placed randomly. */

void place_mines()
{
	int number1;
	int number2;
	
	int i=1;
	int j;
	
	/* Edit to change the amount of mines (30 is standard) */
	int mines = 3; 
	
	
	while (i<=mines)
	{
		number1 = random_number();
		number2 = random_number();
		
		
		if (game_board[number1][number2][0] == ' ')
		{
			game_board[number1][number2][0] = '*';
			i++;
		}
		else 
		{}
	}
	
	for (i=0;i<=11;i++)
	{
		for (j=0;j<11;j++)
		{
			printf("%c",game_board[i][j][0]);
		}
		printf("\n");
	}
	
}

/* This program generates random numbers used by the place_mines() -function to place mines on the gameboard. */

int random_number()
{
	int number;
	
	number = rand() % 10 + 1;

	return number;
}

void hide_mines()
{
	int i=0;
	int j=0;
	
	for (i=1;i<11;i++)
	{
		for (j=1;j<11;j++)
		{
			game_board[i][j][1] = '#';
		}
	}
}

int game_status()
{
	int end=0;
	int i=0;
	int j=0;
	int field_count = 0;
	
	printf("\n");
	
	for (i=0;i<11;i++)
	{
		for (j=0;j<11;j++)
		{
			printf("%c",game_board[i][j][1]);
		}
		printf("\n");
	}
	
	for (i=1;i<11;i++)
	{
		for (j=1;j<11;j++)
		{
			if (  (game_board[i][j][1] == ' ') || ((game_board[i][j][1] >='1') && (game_board[i][j][1] <='8'))) 
			{
				field_count++;
			}
			else if (game_board[i][j][1] == 'F')
			{
				if (game_board[i][j][0] == '*')
				{
					field_count++;
				}
				else
				{}
				
			}	
			else if (game_board[i][j][1] == '#')
			{
				if (game_board[i][j][0] == '*')
				{
					field_count++;
				}
				else
				{}
			}
			
			
			else
			{}
		}
	}
	
	
	if (field_count == 100)
	{
		end=0;
	}
	else
	{
		end = 2;
	}
	return end;
}


/* Takes the input from the player and sends it to the check_for_mines() function */

int player_input()
{
	int cha;
	char row_input=0;
	char column_input=0;
	char flag_input=0;
	int flag = 0;
	int remove_flag = 0;
	int row_loop = 0;
	int column_loop = 0;
	int set_flag_loop = 0;
	int remove_flag_loop = 0;
	
	int end; 

	
	dump_line(stdin);
	
	printf("\n");
	printf("********************HELP********************\n");
	printf("* Number on board = amount of nearby mines *\n");
	printf("* F = Flag | # = Unknown area | * = Mine   *\n");
	printf("* Cordinate 1 = a-j  | Cordinate 2 = 0-9   *\n");
	printf("********************************************\n");
	printf("\n");
	
	

	while (row_loop == 0)
	{
		printf("Enter row (a-j): ");
		while ((cha = getchar()) != '\n')
		{ row_input = cha; }
		if ((row_input >= 'a') && (row_input <= 'j'))
		{
			
			row_loop = 1;
			
		}
		else
		{}
	}
	while (column_loop == 0)
	{
		printf("Enter column (0-9): ");
		while ((cha = getchar()) != '\n')
		{ column_input = cha; }
		if ((column_input >= '0') && (column_input <= '9'))
		{
			column_loop = 1;
		}
		else
		{}
		
	}
	
	while (set_flag_loop == 0)
	{
		printf("Do you want to PLACE a flag (Y/N)?: ");
		while ((cha = getchar()) != '\n')
		{ flag_input = cha; }

		if ((flag_input == 'Y') || (flag_input == 'y'))
		{
			flag = 1;
			set_flag_loop = 1;		
		}
		else if ((flag_input == 'N') || (flag_input == 'n'))
		{
			flag = 0;
			set_flag_loop = 1;
		}
		else 
		{
			printf("Something went wrong. Try again.\n");
		}
	}
	
	if (flag == 0)
	{
	
		while (remove_flag_loop == 0)
		{
			printf("Do you want to REMOVE a flag (Y/N)?: ");
			while ((cha = getchar()) != '\n')
			{flag_input = cha;}
	
			if ((flag_input == 'Y') || (flag_input == 'y'))
			{
				remove_flag = 1;
				remove_flag_loop = 1;		
			}
			else if ((flag_input == 'N') || (flag_input == 'n'))
			{
				remove_flag = 0;
				remove_flag_loop = 1;
			}
			else 
			{
				printf("Something went wrong. Try again.");
			}
		}
	}
	else
	{}
	
	
	end = check_for_mines(row_input,column_input,flag,remove_flag);
	
	return end;
}

/* Function checks for mines and places or removes flags */

int check_for_mines(char char_row_input,char char_column_input,int flag,int remove_flag)
{
	int end;
	int i;
	int j;
	char nearby_mines=48;
	int row_input;
	int column_input;
	
	
	/* I have to convert the char to int here so that I can use the user input in the array as cordinates */
	
	if (char_row_input == 'a')
	{ row_input = 1; }
	else if (char_row_input == 'b')
	{ row_input = 2; }
	else if (char_row_input == 'c')
	{ row_input = 3; }
	else if (char_row_input == 'd')
	{ row_input = 4; }
	else if (char_row_input == 'e')
	{ row_input = 5; }
	else if (char_row_input == 'f')
	{ row_input = 6; }
	else if (char_row_input == 'g')
	{ row_input = 7; }
	else if (char_row_input == 'h')
	{ row_input = 8; }
	else if (char_row_input == 'i')
	{ row_input = 9; }
	else if (char_row_input == 'j')
	{ row_input = 10; }

	if (char_column_input == '0')
	{ column_input = 1; }
	else if (char_column_input == '1')
	{ column_input = 2; }
	else if (char_column_input == '2')
	{ column_input = 3; }
	else if (char_column_input == '3')
	{ column_input = 4; }
	else if (char_column_input == '4')
	{ column_input = 5; }
	else if (char_column_input == '5')
	{ column_input = 6; }
	else if (char_column_input == '6')
	{ column_input = 7; }
	else if (char_column_input == '7')
	{ column_input = 8; }
	else if (char_column_input == '8')
	{ column_input = 9; }
	else if (char_column_input == '9')
	{ column_input = 10; }
	
	
	/* Here the code checks if the use wanted to place/remove a flag or if the input cordinate has a mine. */
	
	if ((flag == 1) && (remove_flag == 0))
	{
		game_board[row_input][column_input][1] = 'F' ;
		end = 2;
	}
	else if ((remove_flag == 1) && (flag == 0))
	{
		game_board[row_input][column_input][1] = '#';
		end = 2;
	}
	else if ((flag==0) && (remove_flag == 0 ))
	{
		if (game_board[row_input][column_input][1] == 'F')
		{
			printf("That spot has a Flag placed on it. Choose another spot. \n");
			end = 2;
		}
		else if (game_board[row_input][column_input][0] == '*')
		{
			printf("\n------------------------------\n");
			for (i=0;i<11;i++)
			{
				for (j=0;j<11;j++)
				{
					printf("%c",game_board[i][j][0]);
				}
				printf("\n");
			}
			end = 1;
		}
		else if (game_board[row_input][column_input][0] = ' ')
		{
			recursion(row_input,column_input);
			end=2;
			
		}
	}	

	
	return end;
}

void recursion(int row_input, int column_input)
{
	char nearby_mines=48;
	int i;
	int j;
	
	
	if (game_board[row_input][column_input][1] == '#')
	{
		for (i=row_input-1;i<row_input+2;i++)
		{
			for (j=column_input-1;j<column_input+2;j++)
			{
				if (game_board[i][j][0] == '*')
				{
					nearby_mines++;
				}
			}
		}
	
		if (nearby_mines == '0')
		{
			game_board[row_input][column_input][1] = ' ';
			
			for (i = row_input - 1; i<row_input + 2; i++)
			{
				for (j = column_input - 1; j<column_input + 2; j++)
				{
					if(i >= 1 && i <= 10 && j >= 1 && j <= 10)
					{	
						recursion(i,j);
					}
				}
			}
		}
		else
		{
			
			game_board[row_input][column_input][1] = nearby_mines;
		}
	}
	
}



void dump_line(FILE * fp) 
{
	int ch;

    while ((ch = fgetc(fp)) != EOF && ch != '\n') {

    /* null body */;

    }

}
