/*
Vector, Count and Sort. 
Implement program 3.cpp which reads course points of stu-dents 
(integers 0, 1, …, 58, 59, 60) from file "points.txt" and change points to grades. 
Maximum points is 60. After that you have to change points to grades so 
that limits are 30, 35, 40, 45 and 50. 
After that program prints the horizontal bars of each grade (figure 3). 
You have to use Vector, Count and Sort.
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

int main()
{
	int array[50];
	int counter=0;
	string name;
	int score, stars;
	char star = '*';
	
	cout << "-Points Program-" << endl;
	
	ifstream file( "points.txt", ios::in );
	

    if( !file )
        cerr << "Cant open " << endl;

	while( file >> name >> score )
    {
		if(score < 30)
		{array[counter] = 0;}
		else if(score < 35)
		{array[counter] = 1;}
		else if(score < 40)
		{array[counter] = 2;}
		else if(score < 45)
		{array[counter] = 3;}
		else if(score < 50)
		{array[counter] = 4;}
		else if(score <= 60)
		{array[counter] = 5;}
			
		counter++;
    }
	
	vector <int> scores (array,array+counter);
	
	cout << "Student scores:"  << endl;
	for (int i=0; i<6; i++)
	{
		cout << i << ": ";
		stars = count(array,array+counter,i);
		for(int j=0;j<stars;j++)
		{
			cout << star;			
		}
		
		cout << endl;
	}


	cout << "-Program has ended--" << endl;
	
	
	return 0;
}