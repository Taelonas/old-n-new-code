/*
4.	List. 
An STL list container is a doubly linked list (figure 4), 
in which each element contains a pointer not only to the next element 
but also to the preceding one. Implement program 4.cpp which gets 
characters (letters) from keyboard one character (letter) at time. 
Space is ending. In program character is inserting into the list so 
that characters are in alphabetical order. Finally program prints 
letter in ascending and descending order. 
There must be space between letters.  
*/

#include <iostream>
#include <list>
#include <algorithm>

using namespace std;

int main(void)
{
list <char> mylist;
char input;
	
	cout << "-- List Program --" << endl;
	while (true)
	{
		cout << "Insert char (space ends): ";
		cin.get(input).ignore(1,'\n');
		
		if(input == ' ')
		{break;}
	
		mylist.push_back(input);
		
		mylist.sort();
	}
	
	cout << "List in ascending order: ";
	for(auto i: mylist)
	{cout << i << ' ';}

	reverse(mylist.begin(),mylist.end());

	cout << endl << "List in descending order: ";
	for(auto i: mylist)
	{cout << i << ' ';}
	
	cout << endl << "--Program Ended--"<<endl;
	return 0;
}