/*
Vector.
Implement a program 6.cpp which contains menu (figure 6). 
You can add and remove book to vector. 
Elements of vector are structures which contains name of book and 
author of book. You can also remove books of vector and print the 
number of books in vector. Sample print is in figure 6. 
You can get 2 points of this task. 
*/

#include <iostream>
#include <vector>
#include <string>

using namespace std;

void menu();

struct books
{
	string bookName;
	string author;
	
	books(string bName, string aName)
	{
		bookName = bName;
		author = aName;
	}
};


int main(void)
{
	string input;
	vector <books> book;
	
	int booksInDB = 0;
	
	
	cout << "********** Welcome to database **********" << endl;
	while (true)
	{
		menu();
		getline(cin, input);
		
		if(input == "e"){break;}
		
		else if(input == "a")
		{
			string bName,aName;
			
			cout << "Please enter the name of the book: ";
			getline(cin, bName);
			
			cout << "Please enter the name of the author: ";
			getline(cin, aName);

			book.push_back(books(bName,aName));
			
			cout << "The book has been added." << endl << endl;
			booksInDB++;
			
		}
		
		else if(input == "b")
		{
			int intInput;
			
			if (booksInDB == 0)
			{cout << "No books in DB." << endl;}
		
			else
			{
				cout << "Books in DB:" << endl;
				for(int i=0; i<book.size(); i++)
				{
					cout << i+1 << " - Author: " << book[i].author << "    Book: " << book[i].bookName << endl; 
				}
				
				cout << "Give number of book to remove (1-" << book.size() << ", or -1 to go back): ";
				cin >> intInput;
				
				cout << endl;
				
				if (intInput == -1)
				{continue;}
			
				else if ((intInput >= 0) && (intInput <= book.size()))
				{
					book.erase(book.begin()+intInput-1);
					booksInDB--;
				}
				
				else
				{cout << "Invalid input when removing book!" << endl;}
			}
		}
		
		else if(input == "c")
		{
			bool confirm = true;
			
			while (confirm == true)
			{
				cout << "Are you sure you want to erase the database (Y/N)?: ";
				getline(cin, input);
				
				if (input == "Y")
				{	
					book.erase(book.begin(),book.end());
					confirm = false;
				}
				else if(input == "N")
				{confirm = false;}
			}
			
		}
		
		else if(input == "d")
		{
			cout << "The number of books curently in database: " << book.size() << endl << endl;
		}
	}
	
	cout << "Program Ended" << endl;
	return 0;
}

void menu()
{
	cout << "a. Add a book." << endl;
	cout << "b. Remove a book." << endl;
	cout << "c. Empty database." << endl;
	cout << "d. Show the number of books currently in database." << endl;
	cout << "e. Quit." << endl;
	cout << endl;
	cout << "Please enter a choise: ";
}