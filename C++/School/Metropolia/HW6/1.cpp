/*
Vector. 
Implement a program 1.cpp where you read a string. 
You program checks if the string or sentence is palindrome 
(https://en.wikipedia.org/wiki/Palindrome). 
Notice that for instance finish sentence �innostunut sonni� is 
palindrome because spaces will be ignored. You have to use vectors, 
push_back and pop_back on your solution (figure 1). 
Program must print the original and reverse string. 
Of course you have to print also the text 
�String is not palindrome� or �String is palindrome�. 
*/

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

int main()
{
	string word;
	
	vector <char> v1;
	vector <char> v2;
	
	cout << "--Palidrome program--" << endl;
	while (true)
	{
		cout << "Write a word (quit = stop program): ";
		getline(cin, word);
		if(word == "quit"){break;} // loop break
		
		
		// Removing whitespace
		for(int i=0;i<word.size();i++)
		{
			if (word[i] == ' ')
			{ word.erase(word.begin()+i);}
		}
		
		// Inserting chars to vectors
		for(int i=0;i<word.size();i++)
		{
			v1.push_back(word[i]);
			v2.push_back(word[i]);
		}
		
		
		// reverse vector 
		reverse(v2.begin(),v2.end());
		
		//Print v1
		cout << "Original string: ";
		for(int i=0;i<v1.size();i++)
		{cout << v1[i];}
		//Print v2
		cout << endl << "Reversed string: ";
		for(int i=0;i<v2.size();i++)
		{cout << v2[i];}
		cout << endl;

		if(v1 == v2)
		{cout << "String is palindrome" << endl;}
		else
		{cout << "String is not palindrome" << endl;}
		
		v1.erase(v1.begin(),v1.end());
		v2.erase(v2.begin(),v2.end());
	}
	
	cout << "-Program has ended--" << endl;
	
	
	return 0;
}

