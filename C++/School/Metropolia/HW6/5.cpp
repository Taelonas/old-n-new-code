/*
5.	List and sort.  
Implement a program 5.cpp which creates a list of 10 random 
integers (use a list) and then puts the list into sorted order. 
This program must print integers before and after sorting. 
Sample print is in figure 5. 
*/

#include <iostream>
#include <list>
#include <cstdlib>

using namespace std;

void menu();

int main(void)
{
list <int> mylist;
int input;
int j=0;
	
	cout << "-- List Program --" << endl;
	
	while(j < 10) 
	{
		input = (rand() % 100) + 1;
		mylist.push_back(input);
		j++;
	}
	
	cout << "Original list:" << endl;
	for(auto i: mylist)
	{cout << i << " ";}

	mylist.sort();

	cout << endl << "Sorted contents:" << endl;
	for (auto i: mylist)
	{cout << i << " ";}


	cout << endl << "--Program Ended--"<<endl;
	return 0;
}

