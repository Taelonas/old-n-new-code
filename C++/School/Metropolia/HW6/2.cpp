/*
Vector, Count and Sort. 
Implement program 2.cpp which reads course 
scores of students (integers 0, 1, 2, 3, 4 and 5) from file "scores.txt" 
and counts the number of different grades. 
After that program prints the horizontal bars of each grade. 
You have to use Vector, Count and Sort.
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

int main()
{
	int array[50];
	int counter=0;
	string name;
	int score, stars;
	char star = '*';
	
	cout << "-Score Program-" << endl;
	
	ifstream file( "scores.txt", ios::in );
	

    if( !file )
        cerr << "Cant open " << endl;

	while( file >> name >> score )
    {
		array[counter] = score;
		counter++;
    }
	
	vector <int> scores (array,array+counter);
	
	cout << "Student scores:"  << endl;
	for (int i=0; i<6; i++)
	{
		cout << i << ": ";
		stars = count(array,array+counter,i);
		for(int j=0;j<stars;j++)
		{
			cout << star;			
		}
		
		cout << endl;
	}


	cout << "-Program has ended--" << endl;
	
	
	return 0;
}