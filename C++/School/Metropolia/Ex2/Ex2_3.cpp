/* 

Write a program with a mother class Animal. Inside it define a name and an age
variables and function set_value(). Then create (sub classes) two bases variables
Zebra and Dolphin which write a message telling the age, the name and giving some
extra information (e.g. place of origin). Sample print is in figure 3.


*/

#include <iostream>
#include <string>

using namespace std;

class Animal {
   
   protected:
	string name;
	int age;
   
   public:
	void set_value(string newName, int newAge)
	{
		name = newName;
		age = newAge;
	}	
};


// Derived class
class Zebra: public Animal 
{
	

	public:
		int getAge()
			{return age;}
			
		string getName()
			{return name;}
			
		string getOrigin()
			{return "Africa";}
			
};

class Dolphin: public Animal 
{

	public:
		int getAge()
			{return age;}
			
		string getName()
			{return name;}
			
		string getOrigin()
			{return "Carribian";}
			
};


int main() 
{
   Zebra horse;
   Dolphin fish;
   
   horse.set_value("Frank",10);
   fish.set_value("Steve",5);
   
   cout << "The Zebra named " << horse.getName() << "is " << horse.getAge() << " old. The Zebra comes from " << horse.getOrigin() << endl;
   cout << "The Dolphin named " << fish.getName() << "is " << fish.getAge() << " old. The Dolphin comes from " << fish.getOrigin() << endl;
	
   return 0;
}