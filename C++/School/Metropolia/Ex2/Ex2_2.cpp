/*
Write a program with a class Mother and an inherited class Daughter. Both of them
should have a method void display() that prints a message (different for mother and
daughter). In the main define a daughter and call the display() method on it. Sample
print is in figure 2.

*/

#include <iostream>
#include <string>

using namespace std;

class Mother {
   
   protected:
	int width;
	int height;
   
   public:
	void display()
	{
		cout << "Mother: Food is ready!" << endl;
	}	
};


// Derived class
class Daughter: public Mother 
{
   public:
	void display()
	{
		cout << "Daughter: C++ is AWESOME!!" << endl;
	}
};



int main() 
{
   Mother mom;
   Daughter girl;
   
   mom.display();
   girl.display();

   return 0;
}