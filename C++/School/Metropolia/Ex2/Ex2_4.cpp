/* 

Suppose we would like to manipulate points in a 2D space (two dimensional space).
It is natural for us to define a class for this purpose. Let's call this class TwoD. Suppose
later on we decide to implement a class to deal with points in a 3D place (three
dimensional space). In TwoD are attributes X and Y. Make the definition for the
ThreeD class using inheritance. Define also two parametric constructor for class
TwoD and three parametric constructor for class ThreeD. Define set- and get-methods
for all properties (fields). Implement main function where you create one 2D point
and after that you create one 3D point. Then you print the value of 3D point instance.
Sample print is in figure 4.


*/

#include <iostream>
#include <string>

using namespace std;


class TwoD {
   
   protected:
		int x, y;
   
	public:
		TwoD();
		TwoD(int newX, int newY);

		void setX(int newX)
		{x = newX;}

		void setY(int newY)
		{y = newY;}

		int getX()
		{return x;}

		int getY()
		{return y;}

};

TwoD::TwoD()
{
	TwoD::x = 0;
	TwoD::y = 0;
}

TwoD::TwoD(int newX, int newY)
{
	TwoD::x = newX;
	TwoD::y = newY;
}


// Derived class
class ThreeD: public TwoD 
{
	protected:
		int z;
	
	public:

		ThreeD();
		ThreeD(int newZ);
		ThreeD(int newX, int newY);
		ThreeD(int newX, int newY, int newZ);

		void setZ(int newZ)
		{z = newZ;}
		
		int getZ()
		{return z;}
	
	
};

ThreeD::ThreeD()
{
	ThreeD::x = 0;
	ThreeD::y = 0;
	ThreeD::z = 0;
}

ThreeD::ThreeD(int newZ)
{
	ThreeD::z = newZ;
}

ThreeD::ThreeD(int newX, int newY)
{
	ThreeD::x = newX;
	ThreeD::y = newY;
	ThreeD::z = 0;
}

ThreeD::ThreeD(int newX, int newY, int newZ):TwoD(newX, newY)
{
	ThreeD::z = newZ;
}


int main() 
{
	TwoD space1(1,1);
	ThreeD space2(1,2,3);
	
	cout << "Cordinates for 3D object are:" << endl;
	cout << "X: " << space2.getX() << endl;
	cout << "Y: " << space2.getY() << endl;
	cout << "Z: " << space2.getZ() << endl;
	
	

   return 0;
}