/* 

Function call operator () overloading in C++. Write a program where you overload
function timesTwo. This function timesTwo gets one parameter which type is int and
double. You have to write two different implementation of function timesTwo. In main
function you call this function with value 2 and 2.2. In function timesTwo you have to
print the value. Sample print is in figure 5.

*/

#include <iostream>
#include <string>

using namespace std;

void timesTwo(int x)
{
	x = 2 * x;
	cout << "Result: " << x << endl;
}

void timesTwo(double x)
{
	x = 2.0 * x;
	cout << "Result: " << x << endl;
}


int main()
{
	timesTwo(2);
	timesTwo(2.2);
	
	return 0;
}