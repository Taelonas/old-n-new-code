/*
. Write a program that defines a class Shape with a two parametric function set_value
that gives value to width and height. Define two subclasses Rectangle and Triangle
both of which calculate with method area the area of the shape. In the main you have
to define two variables a triangle and a rectangle. After that you have to give in rectangle
constructor values 5 and 3. Furthermore you have to give in triangle constructor
values 2 and 5. In the end you have to call first the method area of rectangle and then
you have to call the method area of triangle. Sample print is in figure 1.


*/

#include <iostream>
#include <string>

using namespace std;

// Base class
class Shape {
   
   protected:
	int width;
	int height;
   
   public:
	void set_value(int newWidth, int newHeight)
	{
		width = newWidth;
		height = newHeight;
	}	
};


// Derived class
class Rectangle: public Shape 
{
   public:
	int area() 
	{ 
		return (width * height); 
	}
};

// Another derived class
class Triangle: public Shape 
{
   public:
	int area() 
	{ 
		return ((width * height)/2); 
	}
};

int main() {
   Rectangle Rect;
   Triangle Trian;
 
   Rect.set_value(5,3);
   Trian.set_value(2,5);

   // Print the area of the object.
   cout << "Rectangle area: " << Rect.area() << endl;
   cout << "Triangle area: " << Trian.area() << endl;

   return 0;
}