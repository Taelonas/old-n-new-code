/*

Write a program where you overload function add. In main function are next calls
add(A, B), add(A, D), add(A, B, C), add(D, E, F), add(A, E, B) where values of variables
are A = 1, B = 2, C = 3, D = 1.1, E = 2.2 and F = 3.3. Variables A, B and C are
integer and D, E and F are double. Sample print is in figure 6.

*/

#include <iostream>
#include <string>

using namespace std;

void add(int,int);
void add(int,double);
void add(int,int,int);
void add(double,double,double);
void add(int, double, int);

int main()
{
	int A=1, B=2, C=3;
	double D=1.1, E=2.2, F=3.3;
	
	add(A, B);
	add(A, D);
	add(A, B, C);
	add(D, E, F);
	add(A, E, B);
	
	return 0;
}

void add(int x, int y)
{
	cout << "Result: " << x + y << endl;
}

void add(int x, double y)
{
	cout << "Result: " << x + y << endl;
}

void add(int x, int y, int z)
{
	cout << "Result: " << x + y + z<< endl;
}

void add(double x, double y, double z)
{
	cout << "Result: " << x + y + z << endl;
}

void add(int x, double y, int z)
{
	cout << "Result: " << x + y + z<< endl;
}