#include <iostream>
#include <string>
#include <array>

using namespace std;

/*
1. Implement a class ChargeCard which has one property balance and methods
increaseBalance() and payment(). Implement the default constructor which resets
the value of balance. One parametric constructor set for the balance value of the
parameter value. Please note, that default constructor can implement so that default
constructor calls the parametric constructor. Implement also a test class where you
create one card, put money on the card and do some shopping by charge card.
*/
class ChargeCard
{
	private:
		double balance;
	public:
	
		ChargeCard();
		ChargeCard(double newBalance);
	
		void increaseBalance(double increase)
		{balance += increase;}
		
		void payment(double decrease)
		{balance -= decrease;}
		
		double getBalance()
		{return balance;}
};

ChargeCard::ChargeCard()
{
	ChargeCard::balance = 0;
}

ChargeCard::ChargeCard(double newBalance)
{
	ChargeCard::balance = newBalance;
}

/*
2. Implement a class Student, which have properties studentnumber, firstname and
surname. You have to encapsulate properties and implement public set- and getmethods.
Implement a method, which prints properties of student. 
*/

/*
3. For class Student implement the default constructor and three parametric constructor.
Implement the default constructor so that default constructor calls the three parametric
constructor. Default values are : student numer is 0, first name is "Test" and surename
is also "Test". */
class Student
{
	private:
		int studentnumber;
		string firstname, surname;
		
	public:
		Student(int newStudentnumber, string newFirstname, string newSurname);
		Student();
		
		
		void setStudentnumber(int newStudentnumber)
		{studentnumber = newStudentnumber;}
		
		void setFirstname(string newFirstname)
		{firstname = newFirstname;}
		
		void setSurname(string newSurname)
		{surname = newSurname;}
		
		int getStudentnumber()
		{return studentnumber;}
		
		string getFirstname()
		{return firstname;}
		
		string getSurname()
		{return surname;}
		
		void printInfo();
		
};

// This part does not work as wanted in assignment. Call a constructor from another constructor, without creating another instance of an object? 
Student::Student()
{
	Student(0,"Test", "Test");	
}

Student::Student(int newStudentnumber, string newFirstname, string newSurname)
{
	Student::studentnumber = newStudentnumber;
	Student::firstname = newFirstname;
	Student::surname = newSurname;
}

void Student::printInfo()
{
	cout << "Student ID: " << Student::getStudentnumber() << endl;
	cout << "Firstname: " << Student::firstname << endl;
	cout << "Surname: " << Student::surname << endl << endl;
}

/*
4. Implement a class Test in which you create three students and print properties of
them. You have to use method print.
*/
class Test
{
	
	Student student1;
	Student student2;
	Student student3;

	public:
	Test::Test()
	{
		Student student1(1, "Billy", "Idol");
		Student student2(2, "Bat", "Man");
		Student student3(3, "Tony", "Stark");

		student1.printInfo();
		student2.printInfo();
		student3.printInfo();
	}		
};

/*
5. Implement a class StudentGroup have properties groupcode and contactStudent
which is an object of class Student. You have to encapsulate the properties of class
StudentGroup. You have to implement public set- and get-methods for all properties
of class StudentGroup. Implement two parametric constructor and method which
prints values of properties of StudentGroup.
*/

/*
6. Develop class StudentGroup so that it contains an array which contains references
for all students. Develop the constructor of class StudentGroup so that it needs three
parameters. The third parameter is an array which we just added to the class.

*/

/*
7. Develop class StudentGroup so that it contains new method addStudent(student)
which add the student to the student group.
*/

class StudentGroup : public Student
{
	private:
		string groupcode;
		string contactStudent;
		Student students[10];
		int i = 0;

	public:

		StudentGroup();
		StudentGroup(string newGroupCode, string newContactStudent);
		StudentGroup(string newGroupCode, string newContactStudent, Student newStudents);

		void setGroupcode(string newGroupcode)
		{StudentGroup::groupcode = newGroupcode;}

		void setContactStudent(string newContact)
		{StudentGroup::contactStudent = newContact;}

		string getGroupcode()
		{return StudentGroup::groupcode;}

		string getContactStudent()
		{return StudentGroup::contactStudent;}

		void printInfo()
		{
			cout << "Groupcode: " << groupcode << endl;
			cout << "Contact student: " << contactStudent << endl << endl; 
		}

		void addStudent(Student student)
		{
			StudentGroup::students[i] = student;
			i++;
		}
};

StudentGroup::StudentGroup()
{
	StudentGroup::groupcode = "0000xxxx";
	StudentGroup::contactStudent = "NoContact";
}

StudentGroup::StudentGroup(string newGroupCode, string newContactStudent)
{
	StudentGroup::groupcode = newGroupCode;
	StudentGroup::contactStudent = newContactStudent;
}

StudentGroup::StudentGroup(string newGroupCode, string newContactStudent, Student newStudents)
{
	StudentGroup::groupcode = newGroupCode;
	StudentGroup::contactStudent = newContactStudent;
	StudentGroup::students[i] = newStudents;
	i++;

}



int main()
{
	//Part 1
	ChargeCard card;
	cout << endl << "Card balance: " << card.getBalance() << endl;
	card.increaseBalance(200.0);
	cout << "Card balance: " << card.getBalance() << endl;
	card.payment(50.0);
	cout << "Card balance: " << card.getBalance() << endl;
	
	//Part 2 and 3 
	Student frank;
	
	frank.printInfo();
	frank.setStudentnumber(3);
	frank.setFirstname("Frank");
	frank.setSurname("Sinatra");
	frank.printInfo();
	
	//Part 4
	Test testeri;

	//Part 5, 6 and 7
	StudentGroup frank2("001abc", "Steve Irvin", frank);
	frank2.printInfo();
	
	
	return 0;
}