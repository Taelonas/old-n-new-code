
#include <iostream>

using namespace std;

enum shape_e{CIRCLE,SQUARE,RECTANGLE,INVALID};
enum color_e{RED,GREEN,BLUE};
enum choise_e{SHAPE,COLOR};

class Shape
{
	public:
		virtual void draw()=0;
	
};

class Circle:public Shape
{
	public:
		void draw()
		{cout << "Inside Circle::draw() function." << endl;}
};

class Square:public Shape
{
	public:
		void draw()
		{cout << "Inside Square::draw() function." << endl;}
};

class Rectangle:public Shape
{
	public:
		void draw()
		{cout << "Inside Rectangle::draw() function." << endl;}
};

class Color
{
	public:
		virtual void fill()=0;
	
};

class Red:public Color
{
	public:
		void fill()
		{cout << "Inside Red::fill() function." << endl;}
};

class Green:public Color
{
	public:
		void fill()
		{cout << "Inside Green::fill() function." << endl;}	
};

class Blue:public Color
{
	public:
		void fill()
		{cout << "Inside Blue::fill() function." << endl;}	
};

class AbstractFactory
{
	public:
		virtual Color *getColor(color_e nColor)=0;
		virtual Shape *getShape(shape_e nForm)=0;
};



class ShapeFactory:public AbstractFactory
{
	public:
	
		Shape *getShape(shape_e form)
		{
			Shape *shape = NULL;
			
			switch(form)
			{
				case CIRCLE:
					shape = new Circle();
					break;
				case SQUARE:
					shape = new Square();
					break;
				case RECTANGLE:
					shape = new Rectangle();
					break;
				default:
					shape = NULL;
					break;
			}
			return shape;
		}
	
		Color *getColor(color_e nColor)
		{return NULL;}
};

class ColorFactory:public AbstractFactory
{
	public:
		
		Color *getColor(color_e nColor)
		{
			Color *color = NULL;
			
			switch(nColor)
			{
				case RED:
					color = new Red();
					break;
				case GREEN:
					color = new Green();
					break;
				case BLUE:
					color = new Blue();
					break;
				default:
					color = NULL;
					break;
			}
			return color;
		}
		
		Shape *getShape(shape_e nForm)
		{return NULL;}
};

class FactoryProducer
{
	public:
		static AbstractFactory *getFactory(choise_e nChoise)
		{
			AbstractFactory *choise = NULL;
			
			switch(nChoise)
			{
				case SHAPE:
					choise = new ShapeFactory();
					break;
				case COLOR:
					choise = new ColorFactory();
					break;
				default:
					choise = NULL;
					break;
			}
			return choise;
		}
};

int main(void)
{
	
	AbstractFactory *shapeFactory = FactoryProducer::getFactory(SHAPE);
	
	Shape *shape = shapeFactory->getShape(CIRCLE);
	shape->draw();
	
	Shape *shape2 = shapeFactory->getShape(SQUARE);
	shape2->draw();
	
	Shape *shape3 = shapeFactory->getShape(RECTANGLE);
	shape3->draw();
	
	AbstractFactory *colorFactory = FactoryProducer::getFactory(COLOR);
	
	Color *color1 = colorFactory->getColor(RED);
	color1->fill();
	
	Color *color2 = colorFactory->getColor(RED);
	color2->fill();
	
	Color *color3 = colorFactory->getColor(RED);
	color3->fill();
	
	return 0;
};
