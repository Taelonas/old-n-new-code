
#include <iostream>

using namespace std;

enum shape_e{CIRCLE,SQUARE,RECTANGLE,INVALID};

class Shape
{
	public:
		virtual void draw()=0;
	
};

class Circle:public Shape
{
	public:
		void draw()
		{cout << "Inside Circle::draw() function." << endl;}
};

class Square:public Shape
{
	public:
		void draw()
		{cout << "Inside Square::draw() function." << endl;}
};

class Rectangle:public Shape
{
	public:
		void draw()
		{cout << "Inside Rectangle::draw() function." << endl;}
};

class ShapeFactory
{
	public:
	
	Shape *getShape(shape_e form)
	{
		Shape *shape = NULL;
		
		switch(form)
		{
			case CIRCLE:
				shape = new Circle();
				break;
			case SQUARE:
				shape = new Square();
				break;
			case RECTANGLE:
				shape = new Rectangle();
				break;
			default:
				shape = NULL;
				break;
		}
		return shape;
	}
};

int main(void)
{
	ShapeFactory *shapeFactory = new ShapeFactory();
	
	Shape *shape = shapeFactory->getShape(CIRCLE);
	shape->draw();
	
	Shape *shape2 = shapeFactory->getShape(SQUARE);
	shape2->draw();
	
	Shape *shape3 = shapeFactory->getShape(RECTANGLE);
	shape3->draw();
	
	
	return 0;
};