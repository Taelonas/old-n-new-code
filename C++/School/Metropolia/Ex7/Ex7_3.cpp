#include <iostream>

using namespace std;

class SingleObject
{
	private:
		static SingleObject *instance;
		SingleObject(){};
	
	public:
		static SingleObject getInstance(){return *instance;}
		void showMessage()
		{cout << "Hello World!" << endl;}
};

int main(void)
{
	SingleObject object = SingleObject::getInstance();
	
	object.showMessage();
	
	return 0;
};