/*
Binary operators overloading in C++. Define class Vector which have double types
attributes x, y and z. Implement a program where you overload binary operator +.
Implement getLength which return the length of vector. Furthermore implement setX,
setY and setX which sets the coordinates of vector (x,y,z). Further overload + operator
to add two Vector objects. In main program define three Vector objects A, B and C.
Further set coordinates to vectors A = (1, 1, 1) and B = (3, 3, 3). Then print lengths of
vectors A and B. Further use + operator and add two objects A and B and set the
result to C. At the end print length of vector C. Sample print is in figure 1.
*/

#include <iostream>
#include <math.h> // sqrt()

using namespace std;

class Vector
{
	double x, y, z; // Cordinates
		
	public:
		double getLength(){return sqrt(x*x+y*y+z*z);}
		void setX(double newX){Vector::x=newX;}
		void setY(double newY){Vector::y=newY;}
		void setZ(double newZ){Vector::z=newZ;}
		
	// Overload + operator to add two vector objects
	Vector operator+(const Vector& b)
	{
		Vector vector;
		vector.x = this->x + b.x;
		vector.y = this->y + b.y;
		vector.z = this->z + b.z;
		return vector;
	}
};

int main()
{
	Vector Vector1;
	Vector Vector2;
	Vector Vector3;
	double length = 0.0;
	
	Vector1.setX(1.0);
	Vector1.setY(1.0);
	Vector1.setZ(1.0);
	
	Vector2.setX(3.0);
	Vector2.setY(3.0);
	Vector2.setZ(3.0);
	
	length = Vector1.getLength();
	cout << "The length of Vector 1: " << length << endl;
	
	length = Vector2.getLength();
	cout << "The length of Vector 2: " << length << endl;
	
	Vector3 = Vector1 + Vector2;
	length = Vector3.getLength();
	cout << "The length of Vector 3: " << length << endl;
	
	
	
	return 0;
}