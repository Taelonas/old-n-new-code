/*
Logical operators overloading. Implement the class coordinate in which are two
integer type attributes x and y. In this class is non parametric constructor and two
parametric constructor. Furthermore in this class is method get_xy. Next you have to
overload operators == and &&. In main function you have to create 4 objects with
clause coordinate c1( 1, 1 ), c2( 1, 1 ), c3( 1, 0 ), c4( 0, 1 );. After that you have to
print coordinate values c1, c2, c3 and c4 (figure 6). You have to make comparisons
if (c1 == c2), if (c1 == c3), if (c1 == c4), if (c2 == c3), if (c2 == c4), if (c3 == c4), if (c1
&& c2), if (c1 && c3), if (c1 && c4), if (c2 && c3), if (c2 && c4) and if (c3 && c4). Sample
print is in figure 3. Note! The right-hand operand is passed as an argument.
*/

#include <iostream>
#include <string>

using namespace std;

class Cordinate
{
	int x, y;
	
	public:
		Cordinate();
		Cordinate(int X, int Y){x=X;y=Y;}
		
		int get_xy(int &X, int &Y){X = x; Y = y;}
		
		int operator==(Cordinate ob2);
		int operator&&(Cordinate ob2);
};

int Cordinate::operator==(Cordinate ob2)
{
	if (x == ob2.x && y == ob2.y)
		return 1;
	else
		return 0;
}

int Cordinate::operator&&(Cordinate ob2)
{
	return ((x && ob2.x) && (y && ob2.y));
}
int main()
{
	Cordinate c1(1,1);
	Cordinate c2(1,1);
	Cordinate c3(1,0);
	Cordinate c4(0,1);
	
	if (c1 == c2)
	{cout << "c1 = c2" << endl;}
	else
	{cout << "c1 <> c2" << endl;}

	if (c1 == c3)
	{cout << "c1 = c2" << endl;}
	else
	{cout << "c1 <> c2" << endl;}

	if (c1 == c4)
	{cout << "c1 = c2" << endl;}
	else
	{cout << "c1 <> c2" << endl;}

	if (c2 == c3)
	{cout << "c1 = c2" << endl;}
	else
	{cout << "c1 <> c2" << endl;}

	if (c2 == c4)
	{cout << "c1 = c2" << endl;}
	else
	{cout << "c1 <> c2" << endl;}

	if (c3 == c4)
	{cout << "c1 = c2" << endl;}
	else
	{cout << "c1 <> c2" << endl;}
	
	if (c1 && c2)
	{cout << "c1 = c2" << endl;}
	else
	{cout << "c1 <> c2" << endl;}
	
	if (c1 && c3)
	{cout << "c1 = c2" << endl;}
	else
	{cout << "c1 <> c2" << endl;}
	
	if (c1 && c4)
	{cout << "c1 = c2" << endl;}
	else
	{cout << "c1 <> c2" << endl;}
	
	if (c2 && c3)
	{cout << "c1 = c2" << endl;}
	else
	{cout << "c1 <> c2" << endl;}
	
	if (c2 && c4)
	{cout << "c1 = c2" << endl;}
	else
	{cout << "c1 <> c2" << endl;}
	
	if (c3 && c4)
	{cout << "c1 = c2" << endl;}
	else
	{cout << "c1 <> c2" << endl;}
	
	return 0;
}