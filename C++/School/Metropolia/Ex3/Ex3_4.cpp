/*
Single Inheritance (https://www.tutorialcup.com/cplusplus/inheritance.htm#single-inheritance).
Implement a base class Staff with two private properties char name[50] and int code.
Furthermore class Staff contains two public methods getdata and display. Implement
a derived class Typist which is public. This class contains one private property int
speed and two public methods getdata and display. In base class method getdata
asks name and code (figure 4). In derived class method getdata asks speed (figure
4). In base class method display prints name and code (figure 4). In derived class
method display prints speed (figure 4). In main function you must create one Typist
object. Then you have to ask name, code and speed with getdata methods. Furthermore
you have to print name, code and speed with display methods.
*/

#include <iostream>
#include <string>

using namespace std;

class Staff
{
	char name[50];
	int code;
	
	public:
		void getData();
		void display(){cout << endl << "Display data\nName: " << name << endl << "Code: " << code << endl;}
		
};

void Staff::getData()
{
	cout << "Enter data" << "\n" << "Name: ";
	cin >> name;
	
	cout << "Code: ";
	cin >> code;
}

class Typist: public Staff
{
	private: 
		int speed;
	
	public:
		void getData();
		void display();
		
};

void Typist::getData()
{
	Staff::getData();
	
	cout << "Speed: ";
	cin >> speed;
}

void Typist::display()
{
	Staff::display();	
	cout << "Speed: " << speed << endl;
}

int main()
{
	Typist type;
	type.getData();
	type.display();
	
	return 0;
}