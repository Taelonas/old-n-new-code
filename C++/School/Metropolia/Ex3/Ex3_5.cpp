/*
Hierarchical inheritance (https://www.tutorialcup.com/cplusplus/inheritance.htm#hierarchical-inheritance).
Implement a base class Person, derived class Student and derived class Employee.

The class Person contains properties name, gender and age. It contains also method
getdata and display. Method getdata requests values of properties name, gender
and age (figure 5, input 1 and input 2). Method display print values of properties
name, gender and age (figure 5, output 1 and output 2). 

The class Student contains properties institute and level.
It contains also method getdata and display. 
Method getdata requests values of properties institute and level (figure 5, input 1 and input
2). 
Method display print values of properties institute and level (figure 5, output 1
and output 2). 

The class Employee contains properties company and salary. It contains
also method getdata and display. 
Method getdata requests values of properties company and salary (figure 5, input 1 and input 2). Method display print values of
properties company and salary (figure 5, output 1 and output 2). 

In main program you have to create Student and Employee object. 
Then it prints text “Student” and text “Enter data” (figure 5). 
Furthermore you have to call method getdata of student and print text “Displaying data”.
After that you have to call method display of student.
Then it prints text “Empolyee” and text “Enter data” (figure 5). Furthermore you have
to call method getdata of employee and print text “Displaying data”. After that you have
to call method display of employee. Sample print is in figure 5. 
*/

#include <iostream>
#include <string>

using namespace std;

class Person
{
	string name, gender;
	int age;
	
	public: 
		void getData();
		void display();
};

void Person::getData()
{
	cout << "Enter data" << "\n" << "Name: ";
	cin >> name;
	
	cout << "Gender: ";
	cin >> gender;
	
	cout << "Age: ";
	cin >> age;
}

void Person::display()
{
	cout << endl << "Display data\nName: " << name << endl << "Gender: " << gender << endl << "Age: " << age << endl;
}


class Student: public Person
{
	string institute, level;
	
	public: 
		void getData();
		void display();
};

void Student::getData()
{	
	cout << endl << "Student \n";
	Person::getData();
	
	cout << "Name of College/School: ";
	cin >> institute;
	
	cout << "Level: ";
	cin >> level;
}

void Student::display()
{
	Person::display();
	cout << "Name of College/School: " << institute << endl;
	cout << "Level: " << level << endl; 
}


class Employee: public Person
{
	string company;
	int salary;

	public: 
		void getData();
		void display();
	
};

void Employee::getData()
{
	cout << endl << "Employee \n";
	Person::getData();
	
	cout << "Name of company: ";
	cin >> company;
	
	cout << "Salary: ";
	cin >> salary;
}

void Employee::display()
{
	Person::display();
	cout << "Name of Company: " << company << endl;
	cout << "Salary: " << salary << endl; 
}




int main()
{
	Student student;
	Employee employee;
	
	student.getData();
	student.display();
	
	employee.getData();
	employee.display();
	
	return 0;
}