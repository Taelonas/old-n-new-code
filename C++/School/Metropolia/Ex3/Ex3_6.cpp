/*
Multiple Inheritance. Implement a class Petrol which inherits class Fuel and Liquid.
In https://www.tutorialcup.com/cplusplus/inheritance.htm#multiple-inheritance you’ll
see the idea of multiple Inheritance. In class Liquid is one property specific_gravity
and two methods. Input method request to give value of property specific_gravity.
Output method prints a value of property specific_gravity. In class Fuel is one property
rate and two methods. Input method request to give value of property rate. Output
method prints a value of property rate. In class Petrol are two methods. Input method
of class Petrol refers to input methods of both base classes. Furthermore Output
method of class Petrol refers to output methods of both base classes. In main program
you have to create one petrol object. Then you have to print text "Enter data" and refer
to input method of Petrol class. In the end you have to print text "Displaying data" and
refer to output method of Petrol class. Sample print is in figure 6. 
*/

#include <iostream>
#include <string>

using namespace std;

class Fuel
{
	double rate;
	
	public:
		void getData()
		{
			cout << "Rate(per liter): ";
			cin >> rate;
			
		}
		
		void display()
		{
			cout << "Rate(per liter): " << rate << endl;
		}
	
};

class Liquid
{
	float gravity;
	
	public:
		void getData()
		{
			cout << "Specific gravity: " << gravity;
			cin >> gravity;
		}
		
		void display()
		{
			cout << "Specific gravity: " << gravity << endl;
		}
};


class Petrol: public Fuel, public Liquid
{
	public:
	
		void getData()
		{
			cout << endl << "Enter data" << endl;
			Liquid::getData();
			Fuel::getData();
		}
		
		void display()
		{
			cout << endl << "Displaying data" << endl;
			Liquid::display();
			Fuel::display();
		}
};

int main()
{
	Petrol petrol;
	
	petrol.getData();
	petrol.display();
	
	return 0;
}