/*
Constructor overloading. Implement the class dateT in which constructor accepts
date as a string in format pp/k/vv and pp.kk.vv or as three integer in order day,
month and year. Implement method show, which shows the date in right format. Sample
print is in figure 2. Red dates are parameters of the constructor.
*/

#include <iostream>
#include <string>
#include <stdio.h>

using namespace std;

class dateT
{
	int day, month, year;
	
	public:
		dateT(char *str)
		{
			sscanf(str, "%d%*c%d%*c%d", &day, &month, &year); // %d = integer, %*c = char?
		}
		dateT(int d, int m, int y)
		{
			day = d;
			month = m;
			year = y;
		}
		
		void show()
		{
			cout << day << '/' << month << '/' << year << "\n";
		}
};

int main()
{
	dateT d1("11/1/92");
	dateT d2("12.3.45");
	dateT d3(13,6,35);
	
	d1.show();
	d2.show();
	d3.show();
	
	
	return 0;
}