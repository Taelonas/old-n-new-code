#include <iostream>
#include <list>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class Employee
{
private:
	string name, dept;
	int salary;
	vector <Employee> subordinates;

public:

	Employee(string newName, string newDept, int sal)
	{
		this->name = newName;
		this->dept = newDept;
		this->salary = sal;
		subordinates = vector <Employee>();
	}

	void add(Employee e)
	{
		subordinates.push_back(e);
	}

	void remove(Employee e)
	{
	//	subordinates.remove(e);
	}

	vector <Employee> getSubordinates()
	{
		return subordinates;
	}

	void toString()
	{
		cout << "Employee :[ Name : " << name << ", dept : " << dept << ", salary : " << salary << " ]" << endl;
	}
};

int main(void)
{
	Employee CEO("John", "CEO", 30000);
	Employee headSales("Robert", "Head Sales", 20000);
	Employee headMarketing("Michael", "Head Marketing", 20000);

	Employee clerk1("Laura", "Marketing", 10000);
	Employee clerk2("Bob", "Marketing", 10000);

	Employee salesExecutive1("Richard", "Sales", 10000);
	Employee salesExecutive2("Rob", "Sales", 10000);
	
	CEO.add(headSales);
	CEO.add(headMarketing);

	headSales.add(salesExecutive1);
	headSales.add(salesExecutive2);

	headMarketing.add(clerk1);
	headMarketing.add(clerk2);

	CEO.toString();
	int i = 0;
	for (Employee headEmployee : CEO.getSubordinates())
	{
		headEmployee.toString();

		if (i==0)
		{
			for (Employee lastemployee : headSales.getSubordinates())
			{
				lastemployee.toString();	
			}
			i++;
		}
		else
		{
			for (Employee lastemployee : headMarketing.getSubordinates())
			{
				lastemployee.toString();
			}
		}		
	}
	
	return 0;
};