#include <iostream>
#include <list>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class Shape
{
public:
	virtual void draw() {};
};

class Circle : public Shape
{
public:
	virtual void draw()
	{
		cout << "Shape: Circle" << endl;
	}
};

class Rectangle : public Shape
{
public:
	virtual void draw()
	{
		cout << "Shape: Rectangle" << endl;
	}
};

class ShapeDecorator : public Shape
{
protected:
	Shape *decoratedShape;

public:
	ShapeDecorator();
	ShapeDecorator(Shape *newDecoratedShape)
	{
		this->decoratedShape = newDecoratedShape;
	}

	virtual void draw()
	{
		decoratedShape->draw();
	}
};

class RedShapeDecorator : public ShapeDecorator
{

public:

	RedShapeDecorator();
	RedShapeDecorator(Shape newDecoratedShape2)
	{
		ShapeDecorator(newDecoratedShape2);
	}

	virtual void draw()
	{
		decoratedShape->draw();
		setRedBorder(*decoratedShape);
	}

private:
	void setRedBorder(Shape decoratedShape)
	{
		cout << "Border Color: Red" << endl;
	}
};


int main(void)
{

	Shape *circle = new Circle();

	//Shape *redCirc = new Circle();
	Shape *redCircle = new RedShapeDecorator(Circle());

	//Shape *redRect = new Rectangle();
	Shape *redRectangle = new RedShapeDecorator(Rectangle());

	cout << "Circle with normal border" << endl;
	circle->draw();

	cout << endl;
	cout << "Circle of red border" << endl;
	//redCirc->draw();
	redCircle->draw();

	cout << endl;
	cout << "Rectangle of red border" << endl;
	//redRect->draw();
	redRectangle->draw();

	return 0;
};