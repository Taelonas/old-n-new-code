#include <iostream>
#include <list>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class Shape
{
public:
	virtual void draw() { cout << "Generic shape" << endl; }
};

class Circle : public Shape
{
public:
	virtual void draw()
	{
		cout << "Circle::draw()" << endl;
	}
};

class Rectangle : public Shape
{
public:
	virtual void draw()
	{
		cout << "Rectangle::draw()" << endl;
	}
};

class Square : public Shape
{
public:
	virtual void draw()
	{
		cout << "Square::draw()" << endl;
	}
};


class ShapeMaker
{
private:
	Shape *circle;
	Shape *rectangle;
	Shape *square;

public:
	ShapeMaker()
	{
		circle = new Circle();
		rectangle = new Rectangle();
		square = new Square();
	}

	void drawCircle() { circle->draw(); }
	void drawRectangle() { rectangle->draw(); }
	void drawSquare() { square->draw(); }
};


int main(void)
{
	ShapeMaker *shapemaker = new ShapeMaker();

	shapemaker->drawCircle();
	shapemaker->drawRectangle();
	shapemaker->drawSquare();


	return 0;
};