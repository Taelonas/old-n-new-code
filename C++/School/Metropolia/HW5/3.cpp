/*
3. Implement a program 3.cpp in which you use fputs(). A program reads strings from
the keyboard and writes them to the file called “test.txt”. To terminate the program,
enter a blank line. Since gets() does not store the newline character, one is added
before each string is written to the file so that the file can be read more easily. Sample
output is in figure 3. [Sch03a, p. 222]
*/

#include <iostream>
#include <stdio.h>
#include <string>
#include <algorithm>

using namespace std;

int main(void)
{
	const char *filename = "test.txt";	
	char text[20];
	string exit;

	
	FILE *fp;
	
	
	if ((fp = fopen(filename, "a")) == NULL) 
	{
		printf("opening file %s failed\n", filename);
	}
	
	while (true)
	{
		printf("Enter a string (CR to quit): ");
		gets(text);

		if (text[0] == ' ')
		{break;}
		
		fprintf(fp,"\r\n");
		fputs(text,fp);
		
	}
	
	
	fclose(fp);
	return 0;
}