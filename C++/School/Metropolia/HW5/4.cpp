/*
Implement a program 4.cpp which writes objects into file. Then we generally want to
use binary mode. This writes the same bit configuration to disk that was stored in
memory, and ensures that numerical data contained in objects is handled properly.
This program asks the user for information about an object of class person, and then
writes this object to the disk file “person.dat”. After that program reads an object back
from the “person.dat”. In figure is the listing of program. [Laf02, p. 592-593]
*/

#include <iostream>
#include <string>
#include <fstream>


using namespace std;

class Person
{
	private:
		string fName,sName;
		int age;
	
	public:
		Person()
		{
			fName = "NofName";
			sName = "NosName";
			age = 0;
		}
		
		Person(string newfName, string newsName, int newAge)
		{
			fName = newfName;
			sName = newsName;
			age = newAge;
		}
		
		string getfName(){return fName;}
		string getsName(){return sName;}
		int getAge(){return age;}
		
		void setfName(string newfName){fName=newfName;}
		void setsName(string newsName){sName=newsName;}
		void setAge(int newAge){age=newAge;}
		
		void getData()
		{
			cout << "Enter forename: ";
			cin >> fName;
			cout << "Enter surname: ";
			cin >> sName;
			cout << "Enter age: ";
			cin >> age;
		}
		
		void saveToFile()
		{
			string temp;
			string stringAge = to_string(age);
			temp.append(fName);
			temp.append(" ");
			temp.append(sName);
			temp.append(" ");
			temp.append(stringAge);
			temp.append("\n");
			
			
			ofstream myfile ("person.dat",std::ofstream::binary);
			myfile << temp;
			myfile.close();
			
			cout << "Data saved to the file person.dat" << endl;

		}
		
		void loadFromFile()
		{
			ifstream fin;
			fin.open("person.dat");
			if (!fin) {
				cerr << "Error in opening the file" << endl;
			}

			fin >> fName >> sName >> age;
			
			fin.close();
			cout << "Data loaded from file person.dat" << endl;
		}

};




int main(void)
{
	
	bool program = true;
	int input;
	
	Person person;
	
	while  (program == true)
	{
		cout << endl << "What do you want to do?" << endl;
		cout << "(1) Add person data" << endl;
		cout << "(2) Read current person data" << endl;
		cout << "(3) Save data to file (person.dat)" << endl;
		cout << "(4) Load data from file (person.dat)" << endl;
		cout << "(5) Quit" << endl;
		cout << "?: ";
		cin >> input;

		if (input == 1){person.getData();}
		else if (input == 2)
		{ 
			cout << endl << " -Person Data- " << endl;
			cout << "Forename: " << person.getfName() << endl;
			cout << "Surname: " << person.getsName() << endl;
			cout << "Age: " << person.getAge() << endl;
		}
		
		else if (input == 3){person.saveToFile();}
		else if (input == 4){person.loadFromFile();}
		else if (input == 5){program = false;}
		else {cout << "Invalid input!" << endl;}
		
	}

	cout << endl << "-- Program has ended --" << endl;

	return 0;
}

