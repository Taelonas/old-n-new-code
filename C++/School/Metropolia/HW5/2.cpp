/*
Implement a program 2.cpp in which user gives data and program
delete person’s information from file
http://users.metropolia.fi/~pasitr/2017-2018/TI00AA50-3011/kt/05/phones.txt.
First program requests forename. Second program requests surname.
Then program checks if person’s data are in the file.
If the information can be found the program deletes these information from the file.
If the information can’t be found programs prints “Person’s information can’t be found”.
Sample output is in figure 2.
*/

#include <stdio.h>
#include <string.h>

using namespace std;
struct person 
{
	char forename[20 + 1];
	char surname[20 + 1];
	char phone[20 + 1];
};
int main(void)
{
	struct person personArray[50];
	struct person personArray2[50];
	char chkFname[20 + 1], chkSname[20 + 1];
	const char *filename = "phones.txt";
	int i,j=0, lineCounter,indexesRemoved=0;
	
	
	FILE *fp;

	/* retrive old data from file */
	if ((fp = fopen(filename, "r")) == NULL) 
	{
		printf("opening file %s failed\n", filename);
	}
	fscanf(fp, "%d", &lineCounter);
	for (i = 0; i<lineCounter; i++) 
	{
		fscanf(fp, "%s %s %s",
			personArray[i].forename, personArray[i].surname, personArray[i].phone);
	}
	fclose(fp);

	/* request new information */
	printf("This program deletes userdata.\n");
	printf("Give a forename: ");
	scanf("%s", chkFname);
	printf("Give a surname: ");
	scanf("%s", chkSname);

	for (i = 0; i < lineCounter; i++)
	{
	
		if ((strcmp(personArray[i].forename, chkFname) == 0) && (strcmp(personArray[i].surname, chkSname) == 0))
		{
			indexesRemoved++;			
		}

		else
		{
			personArray2[j] = personArray[i];
			j++;
		}
	}
	
	if (indexesRemoved == 0)
	{
		printf("Person's information can't be found.\r\n");
	}

	lineCounter = lineCounter - indexesRemoved;
	/* information updating succeed */
	if ((fp = fopen(filename, "w")) == NULL) 
	{
		printf("opening file %s failed\n", filename);
	}
	fprintf(fp, "%d\r\n", lineCounter);
	for (i = 0; i<lineCounter; i++) 
	{
		fprintf(fp, "%s %s %s\r\n",personArray2[i].forename, personArray2[i].surname, personArray2[i].phone);
	}
	return 0;
}
