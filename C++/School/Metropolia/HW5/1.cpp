/*
Implement a program 1.cpp which prints content (forenames, surnames and phone 
numbers) from file http://users.metropolia.fi/~pasitr/2017-2018/TI00AA50-3011/kt/05/phones.txt. 
Sample output is in figure 1.
*/

#include <stdio.h>
struct person{
  char forename[20+1];
  char surname[20+1];
  char phone[20+1];
};
int main(void)
{
	struct person personArray[50];
	const char *filename = "phones.txt";
	int i, lineCounter;
	FILE *fp;

	/* retrive old data from file */
	if ((fp = fopen(filename, "r")) == NULL) {
		printf("opening file %s failed\n", filename);
	}
	fscanf(fp, "%d", &lineCounter);
	for (i = 0; i<lineCounter; i++) {
		fscanf(fp, "%s %s %s",
			personArray[i].forename, personArray[i].surname, personArray[i].phone);
	}
	fclose(fp);
	
	printf("%d\n",lineCounter);
	for (i = 0; i < lineCounter; i++)
	{
		printf("%s %s %s\n",personArray[i].forename, personArray[i].surname, personArray[i].phone);
	}

  return 0;
}