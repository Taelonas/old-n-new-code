#include <iostream>
#include <string>
using namespace std;

// your code here
class PrintClass
{
	public:
		void print(string cs);
};

void PrintClass::print(string cs)
{
	cout << cs << endl;
}

 
int main()
{
  char charstring[50];
  cout << "Input character string for printing: ";
  cin.get(charstring, 50);
  PrintClass printer;
  printer.print(charstring);
}