#include <iostream>
#include <string>
using namespace std;

// your code
class Rectangle
{
	private:
		int x, y;
	public:
		int setX(int a)
		{ x = a; };
		int setY(int b)
		{ y = b; };
		
		int rectangleArea();//{return x*y;}
};

int Rectangle::rectangleArea()
{
	return x*y;
}


int main()
{
  bool loop = true;
  int first, second, third;
  Rectangle rect;
  
  while (loop == true)
  {
	cout << endl << "Enter length of rectangle (exit on negative number): ";
	cin >> first;
	cout << "Enter breadth of rectangle (exit on negative number): ";
	cin >> second;
	
	if ((first < 0) || (second < 0))
	{
		cout << "Program has ended." << endl;
		loop = false;
	}
	else
	{
		rect.setX(first);
		rect.setY(second);
		cout << "Area: " << rect.rectangleArea() << endl;
	}
	
  }
  return 0;
} 