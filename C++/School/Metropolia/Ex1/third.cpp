#include <iostream>
using namespace std;

class Calculator
{
  public:

  int Sum(int first, int second);
};

int Calculator::Sum(int first, int second)
{
  int sum =first + second;
  return sum;
}

// your code
class Printer
{
	int first, second, third; 
	public:
		void Print();
};

void Printer::Print()
{
	int first, second, third; 
	std::cout << "Please enter first number: ";
	std::cin >> first;
	std::cout << "Please enter second number: ";
	std::cin >> second;
	Calculator object;
	third = object.Sum(first,second);
	std::cout << "Sum: " << third << endl;
}


int main()
{  
  Printer object;
  object.Print();
}  