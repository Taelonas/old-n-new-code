#include <iostream>
#include <string>
using namespace std;

// your code
class Dog
{
	int age;
	string name, race, voice;
	public:
		Dog();
		Dog(int DogsAge, string DogsName, string DogsRace, string DogsVoice);
		string Bark();
		void PrintInformation();
		
};

Dog::Dog()
{
	name = "Unnamed";
	race = "NoBreed";
	age = 0;
	voice = "NoVoice";
}

Dog::Dog(int newAge, string newName, string newRace, string newVoice)
{
	Dog::age = newAge;
	Dog::name = newName;
	Dog::race = newRace;
	Dog::voice = newVoice;
}

void Dog::PrintInformation()
{
	std::cout << "Age: " << Dog::age << endl;
	std::cout << "Name: " << Dog::name << endl;
	std::cout << "Race: " << Dog::race << endl;
}

string Dog::Bark()
{
	return Dog::voice;
}
int main()
{
  Dog buffy(2, "Buffy", "Bulldog", "Hau!!!");
  buffy.PrintInformation();
  cout << "Dog says: " << buffy.Bark() << endl;
} 