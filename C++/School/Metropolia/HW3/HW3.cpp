#include <iostream>
#include <string>

using namespace std;

/*
Part 1:
Look at the example of classes Figure, Point and Circle made in Java.
Make those clas-ses in C++. After that implement a class Rectangle.
An angle point, width and height are properties of Rectangle.
Insert in the hierarchy a method move(), which moves a figure (means coordinates get different values).
Method move() gets two parametres x and y.
Those parameteres tells a transition.
Think carefully in which hierarchy level is the best to implement that methods.
*/

class Figure
{
	private:
		int x, y;
	
	public:
		void setX(int X){this->x = X;}
		void setY(int Y){this->y = Y;}
		int getX(){return x;}
		int getY(){return y;}
};

class PointXY: public Figure
{
	public:
		void show(){cout << "I'm figure (" << getX() << "," << getY() << ")";}
};

class Circle: public Figure
{
	private:
		int radius;
		
	public:
		void setRadius(int newRadius){this->radius = newRadius;}
		
		void show(){cout << "I'm circle which is " << radius << " and midpoint is (" << Figure::getX() << "," << Figure::getY() << ")";}
};

class Rectangle: public Figure
{
	private:
		double anglePoint; // Implemented, but there is nothing in the HW where we use this.
		int width, height; 
		
	public:
		// move() is from HW3 Part 1
		void move(int X, int Y)
		{
			Figure::setX(X);
			Figure::setY(Y);
		}
		
		/* Part 2
		In figure 2 you’ll see that a grey rectangle is inside the bigger rectangle. Implement a class
		Rectangle which has two properties width and height. Properties width and height must
		be private. Implement also methods set and get and two parametric constructor. Implement
		method area which returns an area of rectangle.
		*/
		
		Rectangle();
		Rectangle(int newWidth, int newHeight)
		{
			this->width = newWidth;
			this->height = newHeight;
		}
		
		void setWidth(int newWidth){this->width = newWidth;}
		void setHeight(int newHeight){this->height = newHeight;}
		
		int getWidth(){return width;}
		int getHeight(){return height;}
		
		int area(){return width * height;}
};

/* Part 3
Implement a class ScreenRectangle which is a subclass of Rectangle. Subclass
ScreenRectangle has two new properties x and y, which are the coordinates of left down
corner. Also x and y are private. Implement four parametric constructor which calls constructor
of superclass to set height and width and sets coordinates of corner left down.
*/

class ScreenRectangle: public Rectangle
{
	private:
		int a, b; // left down corner
		
	public:
		ScreenRectangle();
		ScreenRectangle(int width, int height, int X, int Y)
		{
			Rectangle::setWidth(width);
			Rectangle::setHeight(height);
			a = X;
			b = Y;
		}
		void fit(int X, int Y)
		{
			int Z = X*Y;
			if (Z <= Rectangle::area()){cout << "Rectangle fits in window." << endl;}
			else {cout << "Rectangle does not fit in window." << endl;}			
		}
		
};



/*
Part 4. 
Implement a class RectangleTest in which
* you create from class ScreenRectangle one instance which width is 800, height is
  30 and the coordinates of left down corner is (225,120),
* you have to test with method fit() if rectangle you create fits the screen rectangle.
  You have to create rectangle which size is 1024 x 768
* you have to print the area of rectangle you create and
* you have to create rectangle which width is 80 and height 40
* print the area of rectangle.

	I did main() instead of RectangleTest
*/
int main()
{
	ScreenRectangle stuff(800,30,225,120);
	Rectangle block(1024,768);
	stuff.fit(block.getWidth(),block.getHeight());
	block.area();
	Rectangle block2(80,40);
	block2.area();
	
	
		
	return 0;
}