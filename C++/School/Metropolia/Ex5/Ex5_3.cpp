/*
Implement program 3.cpp which requests user�s forename and surname (figure 4). 
These names will be save into a file which name user gives in program (figure 4). 
The prefix of file name is maximum 8 character. The extension of file name is maximum 3 character. 
The program and the file must be in same folder. Surname can be up to 20 characters long. 
Forename can be up to 15 characters long.
*/


#include <iostream>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;


int main()
{
	char fName[15+1], sName[20+1], fileName[12+1];

	FILE *textFile;

	printf("Program saves your forename and surname into the file.\n");
	printf("Enter your forename (First name): ");
	scanf("%s", fName);
	printf("Enter your surname (Last name): ");
	scanf("%s", sName);
	printf("Enter file name: ");
	scanf("%s", fileName);

	

	if ((textFile = fopen(fileName, "w")) == NULL)
	{
		printf("opening file %s failed \n", fileName);
		fclose(textFile);
		return 0;
	}

	if (fprintf(textFile, "%s %s\n", fName, sName) > 0)
	{
		printf("Saving succeed!\n");
	}

	fclose(textFile);

	return 0;
}