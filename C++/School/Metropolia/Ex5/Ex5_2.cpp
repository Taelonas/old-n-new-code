/*
Implement program 2.cpp which reads integer numbers from file "int.s" 
and counts the sum of integers (figure 2). File "int.s" contains 4 integers (figure 2). 
Integers are in the first line and separated by spaces. 
Finally program prints integers and the sum (figure 3).
*/
#include <cstdio>
#include <cstdlib>

using namespace std;

int main()
{
	char *filename = "int.s";
	int numbers[4],i,sum=0;

	FILE *number;

	if ((number = fopen(filename, "r")) == NULL)
	{
		printf("opening file %s failed \n", filename);
	}

	for (i = 0; i < 4; i++)
	{
		fscanf(number, "%d", &numbers[i]);
		sum += numbers[i];
	}

	fclose(number);
	printf("Numbers in file %s are \n", filename);
	for (i = 0; i < 4; i++)
	{
		if (i == 0)
		{
			printf("%d", numbers[i]);
		}
		else if (i < 3)
		{
			printf(", %d", numbers[i]);
		}
		else
		{
			printf(" ja %d\n", numbers[i]);
		}
	}

	printf("Sum of integer is %d\n", sum);

	return 0;
}
