#include <cstdio>
#include <cstdlib>
/*
Implement a program 1.cpp which writes the text "Hello world!" into the file "hello.usr". 
The file does not exist, it is created. 
Finally, the program will print a screen that writing to a file managed (kuva 1).
Sample output is in figure 3.
*/
int main()
{
	FILE* fp = std::fopen("hello.usr", "w");
	if (!fp) 
	{
		std::perror("File opening failed");
		return EXIT_FAILURE;
	}

	if (fprintf(fp, "Hello World!\n") > 0)
	{
		printf("Wrinting to a file managed.\n");
		printf("The program is closed.\n");
	}

	fclose(fp);

	return 0;
}