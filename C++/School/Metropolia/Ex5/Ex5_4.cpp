/*
Implement a program 4.cpp which reads a text file A.txt and writes characters into the file B.txt. 
Reading and writing must be character at a time.
*/


#include <iostream>
#include <string>
#include <cstdio>
#include <cstdlib>


using namespace std;


int main()
{
	FILE* file1 = fopen("A.txt", "r");
	FILE* file2 = fopen("B.txt", "w");

	if (!file1) {
		printf("File opening failed");
		return 0;
	}

	int c; // note: int, not char, required to handle EOF
	while ((c = fgetc(file1)) != EOF) { // standard C I/O file reading loop
		fputc(c, file2);
	}

	if (ferror(file1))
		puts("I/O error when reading");
	else if (feof(file1))
		puts("End of file reached successfully");

	fclose(file1);
	fclose(file2);

	return 0;
}