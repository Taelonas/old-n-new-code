/*
Implement a program 5.cpp which insert a new name and phone number into the file http://users.metropolia.fi/~pasitr/2017-2018/TI00AA50-3011/tt/05/phones.txt.
First the program requests a first name (forename).
Second the program requests a last name (surname).
Third the program requests a phone number.
On the first line in the file �phones.txt� is the number of lines in the file.
The value must increase by one when forename, surname and phone number are stored in the file.
Input data must not con-tain spacebar.
In the file personal data must be on a separate line (remember newline character).
In the file can be up to 50 names (information lines). First line (counter) is not included.
File content is in the beginning next:
4
Etunimi Sukunimi 050-3500980
Matti Meik�l�inen 041-3478924
Brian Kottarainen 040-3980982
Brita Kottarainen 05-4567393
*/


#include <iostream>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

struct person
{
	char fName[21];
	char sName[21];
	char phone[21];
};

int main()
{

	struct person personArray[50];

	const char *filename = "phones.txt";
	int i, lineCounter;

	FILE *fp;


	if ((fp = fopen(filename, "r")) == NULL)
	{
		printf("Opening file %s failed\n", filename);
	}

	fscanf(fp, "%d", &lineCounter);
	for (i = 0; i < lineCounter; i++)
	{
		fscanf(fp, "%s %s %s", personArray[i].fName, personArray[i].sName, personArray[i].phone);
	}

	fclose(fp);

	printf("Program saves your forename, surname and phonenumber into the phones.txt.\n");
	printf("Enter your forename (First name): ");
	scanf("%s", personArray[lineCounter].fName);
	printf("Enter your surname (Last name): ");
	scanf("%s", personArray[lineCounter].sName);
	printf("Enter phonenumber: ");
	scanf("%s", personArray[lineCounter].phone);

	if ((fp = fopen(filename, "w")) == NULL)
	{
		printf("Opening file %s failed\n", filename);
	}

	fprintf(fp, "%d\n", ++lineCounter);
	for (i = 0; i < lineCounter; i++)
	{
		fprintf(fp,"%s %s %s\n", personArray[i].fName, personArray[i].sName, personArray[i].phone);

	}

	fclose(fp);

	return 0;
}