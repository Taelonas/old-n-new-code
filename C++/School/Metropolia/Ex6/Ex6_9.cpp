/*
Deque. 
You can see the idea of Vector in figure 6. 
A deque is like a vector in some ways and like a linked list in others. 
Like a vector, it supports random access using the [] operator. 
However, like a list, a deque can be accessed at the front as well as the back. 
It’s a sort of double-ended vector, supporting push_front(), pop_front(), and front(). 
Implement a program 9.cpp where you define a list with 
clause deque<int> deq; and push back values 30, 40 and 50. 
After that push front values 20 and 10. 
Then you have to change middle item
*/

#include <iostream>
#include <deque>

using namespace std;

int main(void)
{
	
	deque<int> deq; 
	
	deq.push_back(30);
	deq.push_back(40);
	deq.push_back(50);

	deq.push_front(20);
	deq.push_front(10);
	
	deq.insert(deq.begin()+3,33);
	deq.erase(deq.begin()+2);
	
	for(int i=0;i<deq.size();i++)
	{
		cout << deq[i] << " ";
	}
	
	
	cout << endl;
	return 0;
}