/*
Lists. 
An STL list container is a doubly linked list, 
in which each element contains a pointer not only to the next element 
but also to the preceding one (figure 6). 
Implement a program 8.cpp where you define a list with clause 
list<int> ilist; and push back values 30 and 40. 
After that push front values 20 and 10. 
Finally print all values from front. 
Sample print is in figure 9. 
[Laf02, p. 747]
*/

#include <iostream>
#include <algorithm>
#include <array>
#include <vector> 
#include <list>

using namespace std;

int main(void)
{
	
	list<int> ilist;
	
	ilist.push_back(30);
	ilist.push_back(40);
	
	ilist.push_front(20);
	ilist.push_front(10);
	
	for (auto i: ilist)
	{
		cout << i << " ";
	}
	
	cout << endl;
	return 0;
}