/*
Search. Implement a program 4.cpp which contains arrays 
int source[] = { 11, 44, 33, 11, 22, 33, 11, 22, 44 }; 
and 
int pattern[] = { 11, 22, 33 };. 
Program must clarify the offset of sequence 11, 22, 33. 
You have to use right function (that is you job). 
Sample output is in figure 4. [Laf02, p. 737]
*/

#include <iostream>
#include <algorithm>
#include <array>

using namespace std;

int main(void)
{
	
	int x;
	
	int source[] = { 11, 44, 33, 11, 22, 33, 11, 22, 44 };
	int pattern[] = { 11, 22, 33 };
	
	x = distance(source, search(source, source+9, pattern, pattern+3));
	
	cout << "Match at " << x << endl;
	
	return 0;
}