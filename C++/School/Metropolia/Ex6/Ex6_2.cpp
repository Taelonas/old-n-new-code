/*
Count. 
Implement a program 2.cpp which contains an array 
int arr[] = { 33, 22, 33, 44, 33, 55, 66, 77 };. 
Program must clarify how many times 33 is found in array. 
You have to use right function (that is you job). 
Sample output is in figure 2. [Laf02, p. 736]
*/

#include <iostream>
#include <algorithm>
#include <array>

using namespace std;

int main(void)
{
	
	int arr[] = { 33, 22, 33, 44, 33, 55, 66, 77 };
	int counts;
	
	cout << "Values in array: ";
	for (int i=0; i<8; i++)
	{
		cout << " " << arr[i];
	}
	cout << endl;
	
	counts = count(arr,arr+8,33);
	
	cout << "There are " << counts << " 33's in arr."  << endl;
	return 0;
}