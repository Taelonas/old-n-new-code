/*
Vector, insert and erase. 
Implement a program 7.cpp where you define array 
int arr[] = { 100, 110, 120, 130 };. 
After that put value 115 at the middle of array. 
Further erase element 2. 
You have to use right functions (that is you job). 
Finally display vector contents (figure 8). 
[Laf02, p. 746]
*/


#include <iostream>
#include <algorithm>
#include <array>
#include <vector> 

using namespace std;

int main(void)
{
	int arr[] = { 100, 110, 120, 130 };
	
	vector <int> v;
	
	for (int i=0; i<4; i++)
	{
		v.push_back(arr[i]);
	}
	
	cout << "Before insertion: ";
	for (int i=0; i<v.size(); i++)
	{
		cout << " " << v[i];
	}
	cout << endl;
	
	v.insert(v.begin()+2,115);
	
	cout << "After insertion: ";
	for (int i=0; i<v.size(); i++)
	{
		cout << " " << v[i];
	}
	cout << endl;
	
	v.erase(v.begin()+2);

	cout << "After erasure: ";
	for (int i=0; i<v.size(); i++)
	{
		cout << " " << v[i];
	}
	cout << endl;
	
	
	return 0;
}