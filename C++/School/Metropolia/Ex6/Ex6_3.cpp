/*
Sort. 
Implement a program 3.cpp which contains an array 
int arr[] = {45, 2, 22, -17, 0, -30, 25, 55};. 
Program must sort an array and after that print an array. 
You have to use right function (that is you job). 
Sample output is in figure 3. [Laf02, p. 737]
*/

#include <iostream>
#include <algorithm>
#include <array>

using namespace std;

int main(void)
{
	
	int arr[] = {45, 2, 22, -17, 0, -30, 25, 55};
	
	cout << "Values in array before sort: ";
	for (int i=0; i<8; i++)
	{
		cout << " " << arr[i];
	}
	cout << endl;
	
	sort(arr,arr+8);
	
	cout << "Values in array after sort: ";
	for (int i=0; i<8; i++)
	{
		cout << " " << arr[i];
	}
	cout << endl;
	
	return 0;
}