/*
Find. 
Implement a program 1.cpp which contains an array int arr[] = { 11, 22, 33, 44, 55, 66, 77, 88 };.
Program must clarify the offset of number 33. 
You have to use right function (that is you job). 
Sample output is in figure 1. [Laf02, p. 735]
*/

#include <iostream>
#include <algorithm>
#include <array>

using namespace std;

int main(void)
{
	
	int arr[] = { 11, 22, 33, 44, 55, 66, 77, 88 };
	int p;
	
	cout << "Values in array: ";
	for (int i=0; i<8; i++)
	{
		cout << " " << arr[i];
	}
	cout << endl;
	
	p = distance(arr, find(arr,arr+8,33));
	
	cout << "First object with value 33 found at offset " << p << endl;
	return 0;
}