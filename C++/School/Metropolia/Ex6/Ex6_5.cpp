/*
Merge. 
Implement a program 5.cpp which contains three arrays 
int src1[] = { 2, 3, 4, 6, 8 };, 
int src2[] = { 1, 3, 5 }; and 
int dest[8];. 
Program merges the elements from two source containers into a destination. 
You have to use right function (that is you job). 
Sample output is in figure 5. [Laf02, p. 738]
*/

#include <iostream>
#include <algorithm>
#include <array>

using namespace std;

int main(void)
{
	
	int src1[] = { 2, 3, 4, 6, 8 };
	int src2[] = { 1, 3, 5 }; 
	int dest[8];
	
	cout << endl << "src1: ";
	for (int i=0;i<5;i++)
	{
		cout << src1[i] << " ";
	}
	
	cout << endl << "src2: ";
	for (int i=0;i<3;i++)
	{
		cout << src2[i] << " ";
	}
	
	merge(src1,src1+5,src2,src2+3,dest);
	sort(dest,dest+8);
	
	cout << endl << "dest: ";
	for (int i=0;i<8;i++)
	{
		cout << dest[i] << " ";
	}
	cout  << endl;
	
	return 0;
}