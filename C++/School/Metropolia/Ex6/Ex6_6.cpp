/*
Vector, push_back and size. 
You can see the idea of Vector in figure 6. 
Implement a program 6.cpp where you define vector v. 
After that put values 10, 11, 12 and 13 at the end of array.
Replace first value and third value of array so that first value is 20 and third value is 23. 
Finally display vector contents (figure 7). 
You have to use right functions (that is you job). 
[Laf02, p. 744]
*/

#include <iostream>
#include <algorithm>
#include <array>
#include <vector> 

using namespace std;

int main(void)
{
	vector <int> v;
	int arr[] = {10, 11, 12, 13};
	
	for (int i=0; i<4; i++)
	{
		v.push_back(arr[i]);
	}
	
	v.insert(v.begin(),20);
	v.insert(v.begin()+4,23);
	v.erase(v.begin()+1);
	v.erase(v.begin()+4);
	
	
	for(int i=0;i<v.size();i++)
	{
		cout << v[i] << " ";
	}
	cout << endl;
	
	return 0;
}