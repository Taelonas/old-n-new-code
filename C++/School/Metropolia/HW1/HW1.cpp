#include <iostream>
#include <string>

using namespace std;

class Member
{
	private: // Making variables more secure by making them private
		int id;
		string name;
		double bonusMoney;
	
	public: 
		Member();
		Member(int newID, string newName);
		Member(int newID, string newName, double newBonusMoney);
		void setID(int newID);
		int getID();
		void setName(string newName);
		string getName();
		void setBonusMoney(double newBonusMoney);
		double getBonusMoney();
		void increaseBonusMoney(double increaseValue);
};

// Constructor for instances without values
Member::Member()
{
	Member::id = 0;
	Member::name = "NoName";
	Member::bonusMoney = 0.0;
}

// Constructor for instances with ID and Name values. Bonus money will be 20€
Member::Member(int newID, string newName)
{
	Member::id = newID;
	Member::name = newName;
	Member::bonusMoney = 20.0;
}

// Constructor with all values
Member::Member(int newID, string newName, double newBonusMoney)
{
	Member::id = newID;
	Member::name = newName;
	Member::bonusMoney = newBonusMoney;
}

// Function for setting ID
void Member::setID(int newID)
{
	Member::id = newID;
}

// Function for getting ID
int Member::getID()
{
	return Member::id;
}

// Function for setting name
void Member::setName(string newName)
{
	Member::name = newName;
}

// Function for getting name
string Member::getName()
{
	return Member::name;
}

// Function for setting bonusMoney value
void Member::setBonusMoney(double newBonusMoney)
{
	Member::bonusMoney = newBonusMoney;
}

// Function for getting bonusMoney value
double Member::getBonusMoney()
{
	return Member::bonusMoney;
}

// Function for increasing the bonusMoney value
void Member::increaseBonusMoney(double increaseValue)
{
	Member::bonusMoney += increaseValue;
}



// Main program

int main()
{
	// Printing of instance values could have been simplified by creating a function that does this.
	// But it was out of scope for homework. 
	
	
	// Creating the new instances
	Member firstPerson;
	Member secondPerson;
	
	// Setting properties of firstPerson
	firstPerson.setID(1);
	firstPerson.setName("Sauli");
	firstPerson.setBonusMoney(0.0);
	
	// Setting properties of secondPerson
	secondPerson.setID(2);
	secondPerson.setName("Marie");
	secondPerson.setBonusMoney(0.0);
	
	// Printing firstPerson values
	cout << endl << "Name: " << firstPerson.getName() << endl;
	cout << "ID: " << firstPerson.getID() << endl;
	cout << "Bonus Money: " << firstPerson.getBonusMoney() << endl;
	
	// Printing secondPerson values
	cout << endl << "Name: " << secondPerson.getName() << endl;
	cout << "ID: " << secondPerson.getID() << endl;
	cout << "Bonus Money: " << secondPerson.getBonusMoney() << endl;
	
	// Increase bonus of firstPerson and print values
	firstPerson.increaseBonusMoney(1.0);
	cout << endl << "Name: " << firstPerson.getName() << endl;
	cout << "ID: " << firstPerson.getID() << endl;
	cout << "Bonus Money: " << firstPerson.getBonusMoney() << endl;
	
	// Increase bonus of secondPerson and print values
	secondPerson.increaseBonusMoney(5.0);
	cout << endl << "Name: " << secondPerson.getName() << endl;
	cout << "ID: " << secondPerson.getID() << endl;
	cout << "Bonus Money: " << secondPerson.getBonusMoney() << endl;
	
	// Creating thirdPerson instance
	Member thirdPerson(3, "Leo", 50.0);
	
	// Print thirdPerson values
	cout << endl << "Name: " << thirdPerson.getName() << endl;
	cout << "ID: " << thirdPerson.getID() << endl;
	cout << "Bonus Money: " << thirdPerson.getBonusMoney() << endl;
	
	// Creating fourthPerson instance and printing values
	Member fourthPerson(4, "Taika");

	cout << endl << "Name: " << fourthPerson.getName() << endl;
	cout << "ID: " << fourthPerson.getID() << endl;
	cout << "Bonus Money: " << fourthPerson.getBonusMoney() << endl;
	
	return 0;
}






