#include <iostream>
#include <string>

using namespace std;

/*
Generic Functions. The general form of a template function definition is
Implement a template which starts with clause
template <class X> void swapargs(X &a, X &b)
You have to implement the body of that function which swaps the values of variable a
and variable b. Test your program with values
int i=10, j=20;
double x=10.1, y=23.3;
char a='x', b='z';
Sample print is in figure 4.
*/


template <class X> void swapargs(X &a, X &b)
{
	cout << "Inside swapargs." << endl;
	X c;
	c = a;
	a = b;
	b = c;
	
	
}


int main()
{
	int i=10, j=20, k;
	double x=10.1, y=23.3, z;
	char a='x', b='z', c;
	
	cout << "Original i, j: " << i << ", " << j << endl;
	cout << "Original x, y: " << x << ", " << y << endl;
	cout << "Original a, b: " << a << ", " << b << endl;
	
	swapargs<int>(i,j);
	swapargs<double>(x,y);
	swapargs<char>(a,b);
	
	cout << "Swapped i, j: " << i << ", " << j << endl;
	cout << "Swapped x, y: " << x << ", " << y << endl;
	cout << "Swapped a, b: " << a << ", " << b << endl;
	
	return 0;
	
};