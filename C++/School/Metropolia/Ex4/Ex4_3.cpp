#include <iostream>
#include <string>
#include <sstream>

using namespace std;

/*
3. Pure Virtual Function. Implement abstract class number with 1 protected property
val. Implement one public method setval which requests a value to property val. Make
also one pure virtual function show2. Further implement derived class hextype which
contains public function show().It returns the value of property val in hexadecimal
form. Further implement derived class dectype which contains public function show().
It returns the value of property val in decimal form. Furthermore implement derived
class octtype which contains public function show(). It returns the value of property
val in octal form. In main class define one dectype variable, one octtype variable and
one hextype variable. Make setval method call of each objects and put parameter
value 20 of each call. Furthermore make show method call of each objects. Sample
output is in figure 3.
*/

class Number
{
	protected:
		int val;
		
	public:
		void setVal()
		{
			cout << "Give value: ";
			cin >> val;
		}
		virtual void show() = 0;
};

class Hextype:public Number
{
	public:
		void show()
		{	
			cout << hex << Number::val << endl;
		}
};

class Dectype:public Number
{
	public:
		void show()
		{	
			cout << dec << Number::val << endl;
		}
};

class Octype:public Number
{
	public:
		void show()
		{	
			cout << oct << Number::val << endl;
		}
};

int main()
{
	Hextype hex1;
	Dectype dec1;
	Octype oct1;
	
	hex1.setVal();
	hex1.show();
	
	dec1.setVal();
	dec1.show();
	
	oct1.setVal();
	oct1.show();
	
	return 0;
};







