#include <iostream>
#include <string>

using namespace std;


/*
1. Virtual functions (http://www.learncpp.com/cpp-tutorial/122-virtual-functions/). Im-plement base class base which contains public virtual function vfunc().This function prints text "This is base's vfunc().". Further implement derived class derived1 which contains public function vfunc(). This function prints text " This is derived1's vfunc().".Further implement derived class derived2 which contains public function vfunc().This function prints text " This is derived2's vfunc().". Implement main function where you define variables
base *p, b;
derived1 d1;
derived2 d2;
After that you have to make statements so that the output is like in figure 1.
*/

class Base
{
	public:
	 virtual void vfunc(){cout << "This is base's vfunc()" << endl;}
};

class Derived1:public Base
{
	public:
		void vfunc(){cout << "This is derived1's vfunc()." << endl;}
};

class Derived2:public Derived1
{
	public:
		void vfunc(){cout << "This is derived2's vfunc()." << endl;}
};

int main()
{
	Base *p,b;
	Derived1 d1;
	Derived2 d2;
	
	b.vfunc();
	d1.vfunc();
	d2.vfunc();

	Base &rBase = d1;
	rBase.vfunc();
	
	Base &rBase2 = d2;
	rBase2.vfunc();
	
	return 0;
};