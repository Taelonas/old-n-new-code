#include <iostream>
#include <string>

using namespace std;

/*
2. Abstract Class and Pure Virtual Function. 
Implement abstract class Shape with 1 protected property l. 
Implement one public method getData which requests a value to property l. 
Make also one pure virtual function calculateArea1. 
Further implement de-rived class Square which contains public function calculateArea().
It returns the area of square (l*l). 
Further implement derived class Circle which contains public function calculateArea().
It returns the area of circle (3.14*l*l). 
In main class define one circle and one square type variable.
After that you have to make statements so that the output is like in figure 2.
*/

class Shape
{
	protected:
		int l;
		
	public:
		void getData()
		{
			cin >> l;
		}
		
		virtual double calculateArea() = 0;
};

class Square:public Shape
{
	public:
		double calculateArea(){return Shape::l * Shape::l;}
};

class Circle:public Square
{
	public:
		double calculateArea(){return 3.14 * Square::calculateArea();}
};


int main()
{
	Square square;
	Circle circle;
	
	cout << "Enter length to calculate the area of a square: ";
	square.getData();
	cout << "Area of square: " << square.calculateArea() << endl;
	
	cout << "Enter length to calculate the area of a circle: ";
	circle.getData();
	cout << "Area of circle: " << circle.calculateArea() << endl;
	
	return 0;
};