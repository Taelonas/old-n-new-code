#include <iostream>
#include <string>

using namespace std;

/*
6. Generic stack. Implement generic stack class stack which contains two parameters.
First parameter holds the stack and second parameter is index of top of the stack. You
can use next definitions

StackType stck[SIZE]; // holds the stack
int tos; // index of top-of-stack

You have to make public function stack() which initializes the stack. Further you have
to make public function push() which puts the object on stack. Object is given in parameter.
Further you have to make public function pop() which takes the top object
from stack. In main function you have to create two character stacks. You have to put
characters ‘a’, ‘b’ and ‘c’ on stack s1 and characters ‘x’, ‘y’ and ‘z’ on stack s2. two
test the whole program with next two arrays. Furthermore you have to create two double
stacks. You have to put doubles 1.1, 3.3 and 5.5 on stack ds1 and characters 2.2,
4.4 and 6.6 on stack ds2. Finally you have to print values of each stack in order s1,
s2, ds1 and ds2. Sample print is in figure 5.
*/

const int SIZE = 10;

template <class StackType> class stack {
  StackType stck[SIZE];
  int tos;
 
public:
  stack() {
     tos = 0;
  }
    void push(StackType ob)
    {
      if(tos==SIZE) {
        cout << "Stack is full.\n";
        return;
      }
      stck[tos] = ob;
      tos++;
    }
 
    StackType pop()
    {
      if(tos==0) {
        cout << "Stack is empty.\n";
        return 0; // return null on empty stack
      }
      tos--;
      return stck[tos];
    }
};
 
int main() {
  stack<char> s1, s2;
 
  s1.push('a');
  s2.push('x');
  s1.push('b');
  s2.push('y');
  s1.push('c');
  s2.push('z');
 
  for(int i=0; i<3; i++)
     cout << "Pop s1: " << s1.pop() << "\n";
  for(int i=0; i<3; i++)
     cout << "Pop s2: " << s2.pop() << "\n";
 
  stack<double> ds1, ds2;
 
  ds1.push(1.1);
  ds2.push(2.2);
  ds1.push(3.3);
  ds2.push(4.4);
  ds1.push(5.5);
  ds2.push(6.6);
 
  for(int i=0; i<3; i++)
     cout << "Pop ds1: " << ds1.pop() << "\n";
  for(int i=0; i<3; i++)
     cout << "Pop ds2: " << ds2.pop() << "\n";
 
  return 0;
}