#include <iostream>
#include <string>

using namespace std;

/*
5. Generic sort. Implement generic function bubble which contains two parameters.
First parameter is a pointer to array to be sorted and second parameter is number of
items in array. Implement the body of this function. In main function you have to test
the whole program with next two arrays.
int iarray[7] = {7, 5, 4, 3, 9, 8, 6};
double darray[5] = {4.3, 2.5, -0.9, 100.2, 3.0};
*/

template <class X> void bubble(X *array, int numberOfItems)
{
	cout << "Inside bubble." << endl;
	X c;
	for(int a=1; a<numberOfItems; a++)
	{
		for(int b=numberOfItems-1; b>=a; b--)
		{
			if(array[b-1] > array[b]) 
			{
				c = array[b-1];
				array[b-1] = array[b];
				array[b] = c;
			}
		}
	}
}

int main()
{
	int iarray[7] = {7, 5, 4, 3, 9, 8, 6};
	double darray[5] = {4.3, 2.5, -0.9, 100.2, 3.0};
	
	cout << "Here is unsorted integer array: ";
	for (int i=0;i<7;i++)
	{cout << iarray[i] << ", ";}
	cout << endl;
	
	cout << "Here is unsorted double array: ";
	for (int i=0;i<5;i++)
	{cout << darray[i] << ", ";}
	cout << endl;
	
	bubble(iarray,7);
	bubble(darray,5);
	
	cout << "Here is sorted integer array: ";
	for (int i=0;i<7;i++)
	{cout << iarray[i] << ", ";}
	cout << endl;
	
	cout << "Here is sorted double array: ";
	for (int i=0;i<5;i++)
	{cout << darray[i] << ", ";}
	cout << endl;
	
	return 0;
};