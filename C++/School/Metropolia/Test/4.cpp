#include <iostream>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

class Point
{
	int x, y;

	public:
		void setX(int newX) { Point::x = newX; }
		void setY(int newY) { Point::y = newY; }
		int getX() { return x; }
		int getY() { return y; }

		void operator++(int)
		{
			x = x + 1;
			y = y + 1;

		}
			
		void operator+(const Point& b)
		{
			x = x + b.x;
			y = y + b.x;

		}

		void operator-(const Point& b)
		{
			x = x - b.x;
			y = y - b.x;
		}

		void operator=(const Point& b)
		{

			x = b.x;
			y = b.x;

		}

};

int main(void)
{
	int x, y;

	Point point1, point2, point3;

	cout << "Give a x-cordinate of point1: ";
	cin >> x;
	cout << "Give a y-cordinate of point1: ";
	cin >> y;
	point1.setX(x);
	point1.setY(y);

	cout << "Give a x-cordinate of point2: ";
	cin >> x;
	cout << "Give a y-cordinate of point2: ";
	cin >> y;
	point2.setX(x);
	point2.setY(y);

	cout << "Give a x-cordinate of point3: ";
	cin >> x;
	cout << "Give a y-cordinate of point3: ";
	cin >> y;
	point3.setX(x);
	point3.setY(y);

	cout << endl;
	cout << "point1: (" << point1.getX() << "," << point1.getY() << ")" << endl;
	point1++;
	cout << "point1: (" << point1.getX() << "," << point1.getY() << ")" << endl;
	
	cout << endl;
	cout << "point1: (" << point1.getX() << "," << point1.getY() << ")" << endl;
	cout << "point2: (" << point2.getX() << "," << point2.getY() << ")" << endl;
	point1++;
	point2=point1;
	cout << "point1: (" << point1.getX() << "," << point1.getY() << ")" << endl;
	cout << "point2: (" << point2.getX() << "," << point2.getY() << ")" << endl;
	
	cout << endl;
	cout << "point1: (" << point1.getX() << "," << point1.getY() << ")" << endl;
	cout << "point3: (" << point3.getX() << "," << point3.getY() << ")" << endl;
	point1+point3;
	cout << "point1: (" << point1.getX() << "," << point1.getY() << ")" << endl;
	cout << "point3: (" << point3.getX() << "," << point3.getY() << ")" << endl;
	
	cout << endl;
	cout << "point1: (" << point1.getX() << "," << point1.getY() << ")" << endl;
	cout << "point3: (" << point3.getX() << "," << point3.getY() << ")" << endl;
	point1-point3;
	cout << "point1: (" << point1.getX() << "," << point1.getY() << ")" << endl;
	cout << "point3: (" << point3.getX() << "," << point3.getY() << ")" << endl;

	cout << endl;
	cout << "point1: (" << point1.getX() << "," << point1.getY() << ")" << endl;
	cout << "point2: (" << point2.getX() << "," << point2.getY() << ")" << endl;
	cout << "point3: (" << point3.getX() << "," << point3.getY() << ")" << endl;
	point1=point3;
	point2=point3;
	cout << "point1: (" << point1.getX() << "," << point1.getY() << ")" << endl;
	cout << "point3: (" << point3.getX() << "," << point3.getY() << ")" << endl;	


	return 0;
};