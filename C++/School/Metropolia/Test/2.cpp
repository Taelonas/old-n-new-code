#include <iostream>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

int main(void)
{
	string n;
	int number;
	vector<int>numbers;
	int lowest = 0, highest = 0, lowestCount = 1, highestCount = 1;

	fstream fp;

	fp.open("A.txt", ios::in | ios::binary);

	if(fp.is_open())
	{
	  while(fp >> number)
	  {
		numbers.push_back(number);
		fp.get();
	  }
	}

	fp.close();
	

	for (int i = 0; i < numbers.size(); i++)
	{

		if (numbers[i] == highest)
		{
			highestCount++;
		}
		else if (numbers[i] > highest)
		{
			highest = numbers[i];
			highestCount = 1;
		}

		if (numbers[i] == lowest)
		{
			lowestCount++;
		}
		else if(numbers[i] < lowest)
		{
			lowest = numbers[i];
			lowestCount = 1;
		}

	}

	cout << "In file are " << numbers.size() << " Integers." << endl;
	cout << "Minimum number " << lowest << " exists " << lowestCount << " times." << endl;
	cout << "Maximum number " << highest << " exists " << highestCount << " times." << endl;


	return 0;
};