#include <iostream>
#include <string>

using namespace std;

/*
1. Multiple parameter overloading. Implement a program where the types of parameters
of the overloaded functions add and the number of parameters may be different.
In the first add implementation are two integer parameter. In the second add implementation
are integer parameter and double parameter. In the third add implementation
are three integer parameters. In main function are definitions A = 1, B = 2, C = 3
and D = 1.1. Furthermore in main program are calls add (A,B), add (A,D) and add
(A,B,C). Sample print is in figure 1.

*/

// Part 1 Functions 
int add(int x, int y){return x + y;}
double add(int x, double y){return x + y;}
int add(int x, int y, int z){return x + y + z;}

/*
2. Function overloading. Implement a program where the types of parameters of the
overloaded functions test and the number of parameters may be different. In the first
test implementation is one integer parameter. In the second test implementation is
one float parameter. In the third test implementation are one integer parameter and
one double parameter. In main function are definitions int a = 5 and float b = 5.5.
Furthermore in main program are calls test (a), test (b) and test (a,b). Sample print is
in figure 2. 
*/

// Part 2 Functions
void test(int x){cout << "Integer number: " << x << endl;}
void test(float x){cout << "Float number: " << x << endl;}
void test(int x, double y){cout << "Integer number: " << x << " and float number: " << y << endl;}

/*
3. Overloading Arithmetic Operator. Implement a program where you overload +
operator you add to Time(hh:mm:ss) objects. In figure 3 you can see input and output. 
*/
class Time
{
	private:
		int hours, minutes, seconds;
		
	public:
		Time(){hours = 0;minutes = 0;seconds = 0;}
		Time(int h, int m, int s){hours = h;minutes = m;seconds = s;}
		void getData()
		{	
		
			bool loop = true, h=false, m=false, s=false;
			int Hours,Minutes,Seconds;
			
			while (loop == true)
			{	
				if (h == false)
				{
					cout << "Enter the hour(0-11): ";
					cin >> hours;
					
					if ((hours < 0) || (hours > 11))
					{
						cout << "Input out of bounds. Try again!" << endl;
						continue;
					}
					else{h=true;}
				}
			
				if (m == false)
				{
					cout << "Enter the minute(0-59): ";
					cin >> minutes;
					
					if ((minutes < 0) || (minutes > 59))
					{
						cout << "Input out of bounds. Try again!" << endl;
						continue;
					}
					else{m=true;}
				}
				
				if (s == false)
				{
					cout << "Enter the second(0-59): ";
					cin >> seconds;
					
					if ((seconds < 0) || (seconds > 59))
					{
						cout << "Input out of bounds. Try again!" << endl;
						continue;
					}
					else{s=true;loop=false;}
				}
				
				
			}
		}
		int getHours(){return hours;}
		int getMinutes(){return minutes;}
		int getSeconds(){return seconds;}
		void show(){cout << hours << ':' << minutes << ':' << seconds;}
		
		
		// + operator
		void operator+(const Time& ob2)
		{
			int h=0, m=0, s=0;
			
			s = Time::seconds + ob2.seconds;
			m = Time::minutes + ob2.minutes;
			h = Time::hours + ob2.hours;
	
			if (s > 59){s=0; m+=1;}
			if (m > 59){m-=60; h+=1;}
			
			cout << h << ':' << m << ':' << s;
		}
};


/*
Overloading + operator. Implement a class Plus which consists one attribute int
Array [10], constructor Plus (int InitValue), table printing method void PrintArray ()
and + overloading method Plus operator+ (Plus &AnotherObject). In main program
you create two Plus object with clauses Plus FirstObject (4) and Plus SecondObject
(6). Immediately after that you print the contents of both table an in figure 4 you can
see output. Then you create one Plus object with clause Plus DifferenceObject (0)
and use clause DifferenceObject = FirstObject + SecondObject and after that you
have to print the content of object DifferenceObject. In figure 4 you can see output.
*/

class Plus
{
	private:
		int Array[10];
		
	public:
		Plus(int initValue)
		{	for (int i=0; i<10; i++)
			{
				Array[i] = initValue;
			}
		}
		void printArray()
		{
			for (int i=0; i<10; i++)
			{
				cout << Array[i] << "\t";
			}
			
			cout << endl;
		}
		
		Plus operator+(Plus& AnotherObject)
		{
			Plus object(0);
			int h=0, m=0, s=0;
			
			for (int i=0; i<10; i++)
			{
				object.Array[i] = this->Array[i] + AnotherObject.Array[i];
			}

			return object;
		}
		
};

/*
Overload the ++ and -- unary operator. Implement a class ThreeD which contains
integer attributes x, y, z (3-D coordinates). Implemet non parametric constructor and
three parametric constructor. Implement unary operators ThreeD operator++() and
ThreeD operator--() . Implement also function show() which prints object coordinates.
In main program create one object a and print immediately coordinates with show()
function. Then make clause ++a and print coordinates with show() function.
Furthemore make clause --a and print coordinates with show() function. In figure 5 you
can see output.
*/

class ThreeD
{
	private:
		int x,y,z;
	
	public:
		ThreeD(){x=0;y=0;z=0;}
		ThreeD(int X, int Y, int Z){x=X; y=Y; z=Z;}
		
		ThreeD operator++(){x+=1;y+=1;z+=1;}
		ThreeD operator--(){x-=1;y-=1;z-=1;}
		
		void show(){cout << x << ", " << y << ", " << z << endl;}
};


int main()
{
	// Part 1 variable definitions and calls 
	int A = 1, B = 2, C = 3;
	double D = 1.1;
	cout << "----- Part 1 Start -----" << endl;
	cout << "Result: " << add(A,B) << endl;
	cout << "Result: " << add(A,D) << endl;
	cout << "Result: " << add(A,B,C) << endl;
	cout << "----- Part 1 End -----" << endl;
	
	// Part 2 variable definitions and calls 
	int a = 5;
	float b = 5.5;
	
	cout << "----- Part 2 Start -----" << endl;
	test(a);
	test(a,b);
	cout << "----- Part 2 End -----" << endl;
	
	// Part 3 object creations and calls
	Time time1;
	Time time2;
	
	int testinput;
	
	cout << "----- Part 3 Start -----" << endl;
	cout << "Enter the first time" << endl;
	time1.getData();
	cout << endl;
	
	cout << "Enter the second time" << endl;
	time2.getData();
	cout << endl;
	
	cout << "First time: ";
	time1.show();
	cout << endl << "Second time: ";
	time2.show();
	cout << endl << "Sum of times: ";
	time1 + time2;
	cout << endl;
	cout << "----- Part 3 End -----" << endl;
	
	// Part 4
	cout << "----- Part 4 Start -----" << endl;
	Plus FirstObject(4);
	Plus SecondObject(6);
	
	FirstObject.printArray();
	SecondObject.printArray();
	
	Plus DifferentObject(0);
	DifferentObject = FirstObject + SecondObject;
	DifferentObject.printArray();
	
	
	cout << "----- Part 4 End -----" << endl;
	
	// Part 5
	cout << "----- Part 5 Start -----" << endl;
	
	ThreeD a(1,2,3);
	cout << "Original value of a: "; a.show();
	++a;
	cout << "Value after ++a: "; a.show();
	--a;
	cout << "Value after --a: "; a.show();
	
	cout << "----- Part 5 End -----" << endl;
	
	return 0;
}






